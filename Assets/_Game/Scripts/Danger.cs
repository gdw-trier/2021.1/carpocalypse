using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;

public class Danger : MonoBehaviour
{
    public TMP_Text aiZone;

    public AiSpawner spawner;

    public void Update()
    {
        if (aiZone)
            aiZone.text = (spawner.Zone + 1).ToString();
    }
}