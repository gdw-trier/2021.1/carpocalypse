using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthIndicator : MonoBehaviour
{
    [SerializeField] private GameObject[] stages;
    [SerializeField] private float[] stageThreshold;
    [SerializeField] private float min;
    [SerializeField] private GameObject explosion;

    private Health health;

    private void Awake()
    {
        health = GetComponentInParent<Health>();
        health.OnDie += o => { Instantiate(explosion, transform.position, Quaternion.identity); };
    }

    private void Update()
    {
        float h = health.GetPercentage();
        if (h > min)
        {
            DeactivateAllStages();
            return;
        }

        for (int i = 0; i < stages.Length; i++)
        {
            if (h > stageThreshold[i])
            {
                if (!stages[i].activeSelf)
                {
                    DeactivateAllStages();
                    stages[i].SetActive(true);
                    stages[i].GetComponentInChildren<ParticleSystem>().Play(true);
                }
                break;
            }
        }
    }

    private void DeactivateAllStages()
    {
        foreach (GameObject stage in stages)
        {
            stage.SetActive(false);
        }
    }
}