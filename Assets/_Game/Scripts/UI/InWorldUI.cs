using System;
using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;
using UnityEngine.UI;

public class InWorldUI : MonoBehaviour
{
    [SerializeField] private float visibleDistance = 10f;
    [SerializeField] private SpriteRenderer iconRenderer;
    public float height = 3;
    private Transform player;
    private Health health;

    private void Start()
    {
        player = FindObjectOfType<PlayerInputProvider>()?.transform;

        health = transform.parent.GetComponentInParent<Health>();
    }
    // Update is called once per frame
    void Update()
    {
        if (health && health.health <= 0)
        {
            iconRenderer.enabled = false;
            return;
        }


        Vector3 position = transform.parent.position + Vector3.up * height;
        transform.position = position;

        transform.forward = Camera.main.transform.position - transform.position;

        if (player)
        {
            iconRenderer.enabled = Vector3.Distance(transform.position, player.position) <= visibleDistance;
            return;
        }
        
    }

    private void OnValidate()
    {
        //Vector3 localPosition =Vector3.up * height;
        //transform.localPosition = localPosition;
    }

    public void OnAttachFunc()
    {
        iconRenderer.enabled = false;
        enabled = false;
    }

    public void OnDetachFunc()
    {
        iconRenderer.enabled = true;
        enabled = true;
    }
}
