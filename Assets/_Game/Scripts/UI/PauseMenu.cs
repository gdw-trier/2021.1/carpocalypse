using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject uiParent;
    [SerializeField] private GameObject optionsMenu;
    
    public void PauseGame()
    {
        Time.timeScale = 0f;
        uiParent.SetActive(true);
        
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void ContinueGame()
    {
        Time.timeScale = 1f;
        uiParent.SetActive(false);
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void OpenMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OpenOptionsMenu()
    {
        optionsMenu.SetActive(true);
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
    }
}
