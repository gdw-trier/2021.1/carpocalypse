using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using DG.Tweening;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

public class HighscoreUI : MonoBehaviour
{
    private TextMeshProUGUI tmp;
    public GameObject ScoreAdd;
    public Vector3 start;
    public Vector3 end;
    public GameObject parent;
    
    // Start is called before the first frame update
    void Start()
    {
        tmp = GetComponent<TextMeshProUGUI>();
        GameManager.Instance.highscore.OnChange += SpawnText;
    }

    private void SpawnText(int i)
    {
        if (i > 0)
        {
            if (object.Equals(gameObject, null))
            {
                return;
            }
            GameObject go = Instantiate(ScoreAdd,transform.position,Quaternion.identity,parent.transform);
            RectTransform t = go.GetComponent<RectTransform>();
            go.GetComponent<TMP_Text>().text = "+" + i;
            t.anchoredPosition = start;
            t.DOAnchorPos(end, 1).OnComplete(() => Destroy(go));
        }
    }

    // Update is called once per frame
    void Update()
    {
        tmp.text = GameManager.Instance.highscore.CurrentScore.ToString();
    }

    private void OnDestroy()
    {
        GameManager.Instance.highscore.OnChange -= SpawnText;
    }
}
