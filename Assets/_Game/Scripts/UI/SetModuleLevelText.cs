using System;
using System.Collections;
using System.Collections.Generic;
using Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SetModuleLevelText : MonoBehaviour
{
    [SerializeField] private ModuleLevel levels;
    [SerializeField] private TMP_Text mg;
    [SerializeField] private TMP_Text grenade;
    [SerializeField] private TMP_Text armor;
    [SerializeField] private TMP_Text spike;

    [SerializeField] private Image mgImage;
    [SerializeField] private Image rocketImage;
    [SerializeField] private Image armorImage;
    [SerializeField] private Image spikeImage;
    
    [SerializeField] private List<Sprite> mgIcons;
    [SerializeField] private List<Sprite> rocketIcons;
    [SerializeField] private List<Sprite> armorIcons;
    [SerializeField] private List<Sprite> spikeIcons;

    private void Update()
    {
        mg.text = levels.Mg.ToString();
        grenade.text = levels.GrenadeLauncher.ToString();
        armor.text = levels.Armor.ToString();
        spike.text = levels.Spike.ToString();
        
        mgImage.sprite = mgIcons[levels.Mg - 1];
        rocketImage.sprite = rocketIcons[levels.GrenadeLauncher - 1];
        armorImage.sprite = armorIcons[levels.Armor - 1];
        spikeImage.sprite = spikeIcons[levels.Spike - 1];
    }
}