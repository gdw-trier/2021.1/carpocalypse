using System;
using System.Collections;
using System.Collections.Generic;
using Controls;
using TMPro;
using UnityEngine;

public class DistanceUI : MonoBehaviour
{
    [SerializeField] private TMP_Text tmpText;
    private Transform player;
    public Shader shaderGround;

    public List<Material> materials;
    private float maxValue;

    private void Start()
    {
        player = FindObjectOfType<PlayerInputProvider>().transform;
    }

    private void Update()
    {
        if (player)
        {
            Vector3 pos = player.position;
            float dist = Mathf.Sqrt(pos.x * pos.x + pos.z * pos.z);

            if(dist> maxValue)
            {
                maxValue = dist;
                foreach(Material m in materials)
                {
                    m.SetFloat("Vector1_ff55ee66564946d793158c1a5c384688", maxValue+30);
                }
            }
            tmpText.text = dist.ToString("#");
        }
    }
}
