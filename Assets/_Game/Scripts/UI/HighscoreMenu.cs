using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HighscoreMenu : MonoBehaviour
{
    public TMP_Text firstName;
    public TMP_Text firstValue;
    public TMP_Text secondName;
    public TMP_Text secondValue;
    public TMP_Text thirdName;
    public TMP_Text thirdValue;

    public Highscore score;
    public Color32 color;

    private void Start()
    {
        List<int> scores = score.data.previousScores.OrderByDescending(i => i).Take(3).ToList();
        List<string> names = new List<string>();


        for (int i = 0; i < 3; i++)
        {
            int j = score.data.previousScores.IndexOf(scores[i]);
            names.Add(score.data.previousNames[j]);
        }

        firstName.text = names[0];
        firstValue.text = scores[0].ToString();
        secondName.text = names[1];
        secondValue.text = scores[1].ToString();
        thirdName.text = names[2];
        thirdValue.text = scores[2].ToString();

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void TryAgian()
    {
        SceneManager.LoadScene("Level");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Main()
    {
        SceneManager.LoadScene("MainMenu");
    }
}