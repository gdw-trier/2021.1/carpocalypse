using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;

public class DebugInfo : MonoBehaviour
{
    public TMP_Text distToCenter;
    public TMP_Text health;
    public TMP_Text ais;
    public TMP_Text aiZone;
        
    public Health player;
    public AiSpawner spawner;

    public void Update()
    {
        Vector3 pos = player.transform.position;

        float dist = Mathf.Sqrt(pos.x * pos.x + pos.z * pos.z);
        distToCenter.text = dist >= 1 ? dist.ToString("#") : "0";
        health.text = player.health + " | " + player.maxHealth;

        ais.text = "";
        foreach (Transform enemy in spawner.transform)
        {
            ais.text += enemy.name + " | ";
        }

        if(aiZone)
            aiZone.text = (spawner.Zone + 1).ToString();
    }
}