using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuHelper : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject options;
    [SerializeField] private GameObject credits;
    [SerializeField] private GameObject tutorial;
    public bool sceneLoadComplete = false;

    [Header("Tutorial")] [SerializeField] private List<Sprite> tutorialScreens;
    [SerializeField] private Image tutorialImage;
    [SerializeField] private GameObject tutorialNext;
    [SerializeField] private GameObject tutorialBack;
    [SerializeField] private GameObject tutorialPlay;
    [SerializeField] private GameObject tutorialLoading;

    [SerializeField] private EventSystem eventSystem;

    private int currentTutorialScreen = -1;

    public void StartTutorial()
    {
        mainMenu.SetActive(false);
        //sceneLoadComplete = false;
        tutorial.SetActive(true);
        NextTutorialScreen();
    }

    public void ShowCredits()
    {
        mainMenu.SetActive(false);
        credits.SetActive(true);
    }

    public void ShowOptions()
    {
        mainMenu.SetActive(false);
        options.SetActive(true);
    }

    public void BackToMain()
    {
        credits.SetActive(false);
        options.SetActive(false);
        mainMenu.SetActive(true);
    }

    public void NextTutorialScreen()
    {
        currentTutorialScreen++;

        tutorialImage.sprite = tutorialScreens[currentTutorialScreen];

        tutorialNext.SetActive(currentTutorialScreen < tutorialScreens.Count - 1);
        tutorialPlay.SetActive(currentTutorialScreen == tutorialScreens.Count - 1);
    }

    public void PreviousTutorialScreen()
    {
        currentTutorialScreen--;

        if (currentTutorialScreen < 0)
        {
            mainMenu.SetActive(true);
            tutorial.SetActive(false);
            return;
        }

        tutorialImage.sprite = tutorialScreens[currentTutorialScreen];
        tutorialNext.SetActive(currentTutorialScreen < tutorialScreens.Count - 1);

        tutorialPlay.SetActive(false);
        tutorialLoading.SetActive(false);
    }

    public void ShowHighscore()
    {
        SceneManager.LoadScene("Score");
    }

    public void StartGame()
    {
        tutorialPlay.SetActive(false);
        tutorialLoading.SetActive(true);
        tutorialBack.SetActive(false);
        
        Destroy(eventSystem.gameObject);
        SceneManager.LoadSceneAsync("Level", LoadSceneMode.Additive);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}