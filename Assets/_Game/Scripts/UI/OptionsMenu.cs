using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] private Slider masterVolumeSlider;
    [SerializeField] private Slider sfxSlider;
    [SerializeField] private Slider musicSlider;
    
    [SerializeField] private TMP_Dropdown resolutionDropdown;
    [SerializeField] private TMP_Dropdown fullScreenDropdown;
    [SerializeField] private TMP_Dropdown performanceDropdown;
    [SerializeField] private AudioMixer mixer;
    
    private FullScreenMode selectedFullScreenMode;
    private Resolution currentResolution;

    private void Start()
    {
        InitAudioSliders();
        InitResolutionDropdown();
        InitPerformanceDropdown();
    }

    private void InitAudioSliders()
    {
        float temp = 0f; 
        
        mixer.GetFloat("Master", out temp);
        masterVolumeSlider.SetValueWithoutNotify(temp);

        mixer.GetFloat("SFX", out temp);
        masterVolumeSlider.SetValueWithoutNotify(temp);

        mixer.GetFloat("Music", out temp);
        masterVolumeSlider.SetValueWithoutNotify(temp);
    }

    private void InitResolutionDropdown()
    {
        List<string> resolutions = new List<string>();
        Resolution currentResolution = Screen.currentResolution;
        int selectedIndex = -1;

        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            Resolution resolution = Screen.resolutions[i];
            resolutions.Add(resolution.width + "x" + resolution.height + " @ " + resolution.refreshRate + "hz");

            if (IsEqual(resolution, currentResolution))
            {
                selectedIndex = i;
            }
        }
        
        resolutionDropdown.AddOptions(resolutions);
        resolutionDropdown.SetValueWithoutNotify(selectedIndex);
    }

    private void InitPerformanceDropdown()
    {
        List<string> options = new List<string>();
        int i = 0;
        int selectedIndex = -1;
        foreach(Settings.PerformanceSettings setting in Enum.GetValues(typeof(Settings.PerformanceSettings)))
        {
            options.Add(setting.ToString());

            if (setting == Settings.currentSetting)
            {
                selectedIndex = i;
            }
            
            i++;
        }
        
        performanceDropdown.AddOptions(options);
        performanceDropdown.SetValueWithoutNotify(selectedIndex);
    }

    public void OnMasterVolumeChanged(float value)
    {
        if(isActiveAndEnabled)
            mixer.SetFloat("Master", value);
    }
    
    public void OnSFXVolumeChanged(float value)
    {
        if(isActiveAndEnabled)
            mixer.SetFloat("SFX", value);
    }
    
    public void OnMusicVolumeChanged(float value)
    {
        if(isActiveAndEnabled)
         mixer.SetFloat("Music", value);
    }

    public void OnResolutionChanged()
    {
        currentResolution = Screen.resolutions[resolutionDropdown.value];
        Screen.SetResolution(currentResolution.width, currentResolution.height, selectedFullScreenMode, currentResolution.refreshRate);
    }

    public void OnFullScreenChanged()
    {
        switch (fullScreenDropdown.value)
        {
            case 0: selectedFullScreenMode = FullScreenMode.ExclusiveFullScreen;
                break;
            case 1: selectedFullScreenMode = FullScreenMode.FullScreenWindow;
                break;
            case 2: selectedFullScreenMode = FullScreenMode.Windowed;
                break;
        }
        
        Screen.SetResolution(currentResolution.width, currentResolution.height, selectedFullScreenMode, currentResolution.refreshRate);
    }

    public void OnPerformanceSettingChanged()
    {
        Settings.currentSetting = (Settings.PerformanceSettings) performanceDropdown.value;
    }

    private bool IsEqual(Resolution a, Resolution b)
    {
        return a.width == b.width && a.height == b.height;
    }

    public void CloseOptionsMenu()
    {
        gameObject.SetActive(false);
        MainMenuHelper mainMenuHelper = FindObjectOfType<MainMenuHelper>();
        mainMenuHelper?.BackToMain();
    }

    public void OnSensitivityChanged(float value)
    {
        Settings.sensitivity = value;
    }
}
