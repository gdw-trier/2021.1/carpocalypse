using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    public enum PerformanceSettings
    {
        Low = 0, Medium = 1, High = 2, Ultra = 3
    }

    public static PerformanceSettings currentSetting = PerformanceSettings.High;

    public static float sensitivity = 1f;
}
