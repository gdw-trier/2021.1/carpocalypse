using System;
using UnityEngine;

public class Ram : MonoBehaviour
{
    [SerializeField] private float minVelocity;
    [SerializeField] private float maxVelocity;
    [SerializeField] private int damage;
    [SerializeField] private AnimationCurve damageOverVelocity;
    [SerializeField] private GameObject indicator;

    private Rigidbody rb;
    private Team team;

    private void Awake()
    {
        rb = GetComponentInParent<Rigidbody>();
        team = GetComponentInParent<Health>().teamSetup.team;
    }

    private void OnTriggerEnter(Collider other)
    {
        float strength = rb.velocity.magnitude;
        if (strength < minVelocity) return;

        Health health= other.gameObject.GetComponentInParent<Health>();
        if (health)
        {
            //Debug.Log("Strength: " + strength);
            strength = Mathf.Min(strength, maxVelocity);
            int amount = Mathf.RoundToInt(damageOverVelocity.Evaluate(strength / maxVelocity) * damage);
            health.Change(-amount, team);
            if(indicator)
                Instantiate(indicator, transform.position, Quaternion.identity);
        }
    }
}