using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SFXPlayer : MonoBehaviour
{
    private List<AudioSource> audio;
    public AudioClip[] clips;
    public AudioMixerGroup mixer;
    

    private void Start()
    {
        audio = new List<AudioSource>();

    }

    public void Play()
    {
        if (audio == null) return;
        AudioSource src = GetAudio();
        src.clip = clips[Random.Range(0, clips.Length)];
        src.Play();
    }

    public AudioSource GetAudio()
    {

        foreach (AudioSource src in audio)
        {
            if (!src)
                continue;
            if (!src.isPlaying) return src;
        }

        AudioSource srcNew = gameObject.AddComponent<AudioSource>();
        srcNew.spatialBlend = 1;
        srcNew.outputAudioMixerGroup = mixer;
        audio.Add(srcNew);
        return srcNew;
    }
}
