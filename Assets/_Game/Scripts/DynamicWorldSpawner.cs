using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicWorldSpawner : MonoBehaviour
{
    public GameObject[] prefabs;
    public float minRadius;
    public float maxRadius;
    public float despawnRadius;
    public LayerMask terrainLayer;
    public int count;

    List<GameObject> spawnedObjects = new List<GameObject>();

    Transform playerTransform;
    private void Start()
    {
        playerTransform = FindObjectOfType<Controls.PlayerInputProvider>().transform;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (spawnedObjects.Count < count)
        {
            SpawnObject(prefabs[Random.Range(0, prefabs.Length)], true);
        }
        CheckObjects();
    }

    public void SpawnObject(GameObject goPrefab,bool instantiate = true)
    {
        Vector3 playerPos = playerTransform.position;

        Vector3 dir = new Vector3(Random.Range(-1, 1f), 0, Random.Range(-1, 1f));
        dir.Normalize();

        Vector3 spawnPos = playerPos + (dir*Random.Range(minRadius,maxRadius));

        if(!Physics.Raycast(spawnPos + Vector3.up * 20, Vector3.down,out RaycastHit hit, 50, terrainLayer))
            return;

        spawnPos = hit.point + Vector3.up;
        GameObject go;
        if (instantiate)
        {
            go = Instantiate(goPrefab, spawnPos,Quaternion.identity);
        }
        else
        {
            goPrefab.transform.position = spawnPos;
            go = goPrefab;
        }

        if (!spawnedObjects.Contains(go))
            spawnedObjects.Add(go);

    }

    public void CheckObjects()
    {
        for(int i = 0; i < spawnedObjects.Count; i++)
        {
            if (!spawnedObjects[i])
            {
                spawnedObjects.RemoveAt(i);
                continue;
            }
            Vector3 pos = spawnedObjects[i].transform.position;
            float distance = Vector3.Distance(pos, playerTransform.position);
            if (distance > despawnRadius)
            {
                SpawnObject(spawnedObjects[i], false);
            }
        }
    }
}
