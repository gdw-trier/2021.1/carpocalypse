using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "Data/TeamSetups", fileName = "TeamSetups")]
    public class TeamSetup : ScriptableObject
    {
        public LayerMask teamLayer;
        public LayerMask enemyLayer;
        public Team team = Team.ENEMY;
    }

}