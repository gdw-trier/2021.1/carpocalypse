using System.Collections;
using System.Collections.Generic;
using Data;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/EnemyZone", fileName = "EnemyZone")]
public class EnemyZone : ScriptableObject
{
    public float startingDistance;
    
    public int minEnemyCount;
    public int maxEnemyCount;

    public List<CarBlueprint> spawnedEnemies;
}
