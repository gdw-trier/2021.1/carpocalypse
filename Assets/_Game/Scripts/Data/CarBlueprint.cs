using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Data
{
    [CreateAssetMenu(menuName = "Data/CarBlueprints", fileName = "CarBlueprints")]
    public class CarBlueprint : ScriptableObject
    {
        public GameObject[] modules;

        public int killReward = 0;
    }
}