using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "Data/ModuleLevels",fileName = "ModuleLevels")]
    public class ModuleLevel : ScriptableObject
    {
        public int Mg;
        public int GrenadeLauncher;
        public int Armor;
        public int Spike;
    }
}
