using MapGeneration;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LoadingIndicator : MonoBehaviour
{
    public TMP_Text text;

   
    // Update is called once per frame
    void Update()
    {
        if(text && BiomManager.current)
            text.text = "" + Mathf.RoundToInt(Mathf.Clamp01(BiomManager.current.worldGenerator.starting) * 100) + "%";
    }
}
