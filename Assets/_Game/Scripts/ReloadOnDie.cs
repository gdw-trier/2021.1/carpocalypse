using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadOnDie : MonoBehaviour
{
    private void Start()
    {
        if (TryGetComponent(out Health health))
            health.OnDie += ReloadScene;
    }
    public void ReloadScene(GameObject go)
    {
        SceneManager.LoadScene("Gameover");
    }
}
