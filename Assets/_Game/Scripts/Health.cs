using System;
using System.Security.Cryptography;
using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
    [SerializeField] public int maxHealth;
    [SerializeField] private int startHealth;
    [SerializeField] private float immuneDuration = 1;

    public int health;
    private float immuneTimer;
    public bool destroyOnDie = true;
    public event Action<GameObject> OnDie;
    public float destroyDelay = 0.2f;
    public Data.TeamSetup teamSetup;
    public AudioSource src;
    public bool canDie = true;
    public Health parentHealth;

    Outline outline;


    private void Awake()
    {
        health = startHealth;
        immuneTimer = immuneDuration;
        outline = GetComponent<Outline>();
    }

    private void Start()
    {
        if (outline)
            outline.enabled = false;
    }

    public bool Change(int amount, Team team = Team.NONE)
    {
        if (parentHealth)
            parentHealth.Change(amount, team);

        if(immuneTimer > 0 || Mathf.Abs(amount) <= 0) return false;
        if (team == teamSetup.team && amount < 0) return false;
        if(amount < 0 && src != null) src.Play();
        Debug.Log("Damaged : " + gameObject.name + "(" + amount + ")");
        health += amount;
        health = Mathf.Clamp(health, 0, maxHealth);
        immuneTimer = immuneDuration;
        if (health <= 0) Die();

        if (outline && amount < 0)
            StartCoroutine(DamageEffect(0.1f));

        return true;
    }

    private IEnumerator DamageEffect(float duration)
    {
        if (outline)
            outline.enabled = true;
        yield return new WaitForSeconds(duration);
        if (outline)
            outline.enabled = false;
    }

    private void Die()
    {
        if (!canDie)
        {
            health = startHealth;
            return;
        }
        if (teamSetup.team == Team.ENEMY)
        {
            CarModuleManager moduleManager = GetComponent<CarModuleManager>();
            int killReward = moduleManager ? moduleManager.blueprint.killReward : 0;
            GameManager.Instance.OnEnemyKilled(killReward);
        }
        
        OnDie?.Invoke(gameObject);
        if (destroyOnDie)
            Destroy(gameObject, destroyDelay);
    }

    private void Update()
    {
        if (immuneTimer > 0) immuneTimer -= Time.deltaTime;
    }

    public float GetPercentage()
    {
        return health / (float) maxHealth;
    }
}

public enum Team
{
    NONE,
    PLAYER,
    ENEMY
}
