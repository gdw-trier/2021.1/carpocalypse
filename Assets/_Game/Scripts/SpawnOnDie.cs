using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class SpawnOnDie : MonoBehaviour
{
    public GameObject prefab;
    [Range(0, 1)] public float chance;

    void Start()
    {
        GetComponent<Health>().OnDie += o =>
        {
            if (Random.Range(0f, 1f) < chance)
            {
                GameObject go = Instantiate(prefab, transform.position + Vector3.up * 3, Quaternion.identity);
                go.GetComponent<Rigidbody>()?.AddForce(new Vector3(Random.Range(0, 0.5f), 4, Random.Range(0, 0.5f)), ForceMode.Impulse);
            }
        };
    }
}