using System;
using Controls;
using UnityEngine;
using UnityEngine.AI;

namespace AI
{
    public class NavMeshAI : MonoBehaviour, IInputProvider
    {
        [SerializeField] private Transform targetTransform;
        [SerializeField] protected AIPathfinder aiPathfinder;

        [SerializeField] private float reverseDistance = 15f;
        [SerializeField] private float minDistance;

        private Rigidbody rb;

        public event Action<Vector2> OnMove;
        public event Action<Vector2> OnLook;
        public event Action<float> OnUse;
        public event Action<float> OnBrake;
        public event Action OnSpecial;
        public event Action OnPause;

        private float _forceReverseTime = 1f;

        // Start is called before the first frame update
        protected virtual void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            if (!targetTransform) return;

            float forwardAmount = 0f;
            float turnAmount = 0f;

            if (_forceReverseTime > 0f)
            {
                forwardAmount -= 1f;
                turnAmount = 0f;
                _forceReverseTime -= Time.deltaTime;

                if (_forceReverseTime <= 0f)
                {
                    targetTransform.position = transform.position;
                }
            }
            else
            {
                float reachedTargetDistance = 7f;
                float distanceToTarget = Vector3.Distance(transform.position, targetTransform.position);

                if (distanceToTarget > reachedTargetDistance)
                {
                    Vector3 dirToMovePosition = (targetTransform.position - transform.position).normalized;
                    float dot = Vector3.Dot(transform.forward, dirToMovePosition);

                    if (dot > 0)
                    {
                        // Target in front
                        forwardAmount = 1f;
                    }
                    else
                    {
                        // Target behind
                        if (distanceToTarget > reverseDistance)
                        {
                            // Too far to reverse
                            forwardAmount = 1f;
                        }
                        else
                        {
                            forwardAmount = -1f;
                        }
                    }

                    float angleToDir = Vector3.SignedAngle(transform.forward, dirToMovePosition, Vector3.up);

                    if (Mathf.Abs(angleToDir) < 90f)
                        turnAmount = angleToDir / 45f;
                    else
                    {
                        turnAmount = angleToDir > 0 ? 1f : -1f;
                    }
                }
                else
                {
                    forwardAmount = 0f;
                    turnAmount = 0f;
                }
            }

            if (Math.Abs(turnAmount) > 20f)
                forwardAmount *= 1 - Math.Abs(turnAmount);

            Vector2 inputs = new Vector2(turnAmount, forwardAmount);
            Debug.Log("TurnAmount: " + turnAmount + "   | ForwardAmount: " + forwardAmount);
            OnMove?.Invoke(inputs);

            /*
            float brake = 0f;
            if (rb.velocity.x > 0.1f && forwardAmount < 0f)
                brake = 1f;
            else if (rb.velocity.x < -0.1f && forwardAmount > 0f)
            {
                brake = 1f;
            }
            OnBreak?.Invoke(brake);
            */
        }

        public void ForceReverse()
        {
            Debug.Log("ForceReverse");
            _forceReverseTime = .5f;
        }

        public virtual void OnWakeup(Transform target)
        {
            
        }
    }
}