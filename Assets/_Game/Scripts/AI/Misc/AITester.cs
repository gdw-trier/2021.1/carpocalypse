using System;
using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;

namespace AI
{
    public class AITester : MonoBehaviour
    {
        [SerializeField] private float speed = 1f;
        private IInputProvider provider;
        private Vector2 input;

        private void Awake()
        {
            provider = GetComponent<IInputProvider>();
            provider.OnMove += vector2 => input = vector2;
        }

        private void FixedUpdate()
        {
            transform.position = Vector3.MoveTowards(
                transform.position, 
                transform.position + new Vector3(input.x, 0f, input.y), 
                speed);
        }
    }
}
