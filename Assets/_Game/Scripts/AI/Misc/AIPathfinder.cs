using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AI
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class AIPathfinder : MonoBehaviour
    {
        [SerializeField] private float maxDistance = 20f;
        [SerializeField] private Transform aiTransform;
        [SerializeField] private NavMeshAI simpleAI;
        
        [Tooltip("Target to pathfind towards")]
        [SerializeField] private Transform targetTransform;
        
        private NavMeshAgent _navMeshAgent;

        private bool _isChasing = true;

        void Start()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();

            if (!aiTransform)
            {
                Debug.LogWarning("Pathfinding for " + transform.parent.name + " is missing AI script reference.");
                enabled = false;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!targetTransform) 
                return;

            float distanceToAI = 0f; //Vector3.Distance(transform.position, aiTransform.position);

            if (distanceToAI <= maxDistance)
            {
                _navMeshAgent.isStopped = false;

                // Chase target
                if (_isChasing)
                {
                    _navMeshAgent.SetDestination(targetTransform.position);
                }
                // Drive away from target
                else
                {
                    Vector3 fleeDest = targetTransform.position - transform.position;
                    _navMeshAgent.SetDestination(fleeDest);
                }
            }
            else
            {
                _navMeshAgent.isStopped = true;
            }
        }

        public void SetTargetTransform(Transform target)
        {
            targetTransform = target;
        }

        public float GetDistToTarget()
        {
            return Vector3.Distance(targetTransform.position, aiTransform.position);
        }

        public void SetIsChasing(bool chase)
        {
            _isChasing = chase;
        }
    }
}
