using System;
using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;

namespace AI
{
    enum AttackType
    {
        Melee,
        Ranged
    }

    [RequireComponent(typeof(Rigidbody))]
    public class DynamicAI : MonoBehaviour, IInputProvider
    {
        [SerializeField] private float fleeRange = 25f;

        [Tooltip("Uses transform with PlayerInputProvider if not asigned")] [SerializeField]
        private Transform targetTransform;

        [Tooltip("Distance to drive backwards, if higher turns around and drives forwards")] [SerializeField]
        private float reverseDistance = 15f;

        private AttackType attackType;

        // Should be lower-equal than steeringAngle in CarController.cs
        private const float MAX_STEERING_ANGLE = 40f;

        [Tooltip("Limits max turn change per FixedUpdate")] [SerializeField]
        private float maxTurnChange;

        private Vector2 currentInput;

        private Rigidbody rb;
        public CarModuleManager moduleManager;
        private float distanceToTarget;

        #region Collision Avoidance

        [Header("Local Collision Avoidance")] [SerializeField]
        private bool useLocalAvoidance;

        [SerializeField] private LayerMask detectionLayerMask;
        [SerializeField] private float avoidWeightScale = 1f;
        [SerializeField] private int nrRaycasts = 16;
        [SerializeField] private float raycastDistance = 100f;
        [SerializeField] private float maxRaycastDegree = 90f;

        [Tooltip("Enable to draw collision raycasts")] [SerializeField]
        private bool drawDebug;

        [Header("Force Reverse (when stuck)")] [SerializeField]
        private bool useForceReverse;

        [SerializeField] private bool forceReverse;
        [SerializeField] private float forceReverseDist = 3f;
        [SerializeField] private float forceReverseDuration = 2f;

        [SerializeField] private List<MeshRenderer> mainMaterialMeshes;
        [SerializeField] private List<MeshRenderer> tireMaterialMeshes;

        #endregion

        private bool shouldDespawn;
        [SerializeField] private MeshRenderer renderer; 
        public AiSpawner aiSpawner;
        public int spawnZone = -1;

        #region Input Events

        public event Action<Vector2> OnMove;
        public event Action<Vector2> OnLook;
        public event Action<float> OnUse;
        public event Action<float> OnBrake;
        public event Action OnSpecial;
        public event Action OnPause;

        #endregion

        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody>();

            targetTransform = FindObjectOfType<PlayerInputProvider>().transform;
            moduleManager = GetComponent<CarModuleManager>();

            CarModuleSlot[] slots = GetComponentsInChildren<CarModuleSlot>();

            foreach (CarModuleSlot slot in slots)
            {
                slot.OnAttach += OnModuleAttached;
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!targetTransform) return;
           
            if (forceReverse)
            {
                OnMove?.Invoke(new Vector2(0f, -1f));
                return;
            }

            float turnAmountAvoid = 0f;
            float avoidWeight = 0f;

            if (useLocalAvoidance)
            {
                CollisionAvoidance(out avoidWeight, out float angle);
                if (avoidWeight > 0f)
                {
                    // Scales avoidWeight to make it higher than seekWeight
                    avoidWeight *= avoidWeightScale;

                    // Invert turning angle (90° vector to obstacle = 0° turn angle)
                    //angle = angle > 0f ? 90f - angle : -90f + angle;

                    // Clip turnAmount to 0 to 1
                    turnAmountAvoid = Mathf.Clamp(angle / MAX_STEERING_ANGLE, -1f, 1f);
                }
            }

            distanceToTarget = Vector3.Distance(transform.position, targetTransform.position);

            Vector3 dirToMovePosition = (targetTransform.position - transform.position).normalized;
            
            // Ranged enemies flee when they're too close to target
            if (attackType == AttackType.Ranged && distanceToTarget < fleeRange)
                dirToMovePosition *= -1;

            if (shouldDespawn)
            {
                if ((distanceToTarget > 100f && !renderer.isVisible) || distanceToTarget > 200f)
                {
                    DespawnEnemy();
                }
            }
            
            float dot = Vector3.Dot(transform.forward, dirToMovePosition);

            float forwardAmount = dot > 0 || distanceToTarget > reverseDistance ? 1f : -1f;

            float turnAngle = Vector3.SignedAngle(transform.forward, dirToMovePosition, Vector3.up);
            float turnAmount = Mathf.Clamp(turnAngle / MAX_STEERING_ANGLE, -1f, 1f);

            // Calculate final inputs
            // By using average turn amount scaled by weight
            float desiredTurnAmount = (turnAmount * 1f + turnAmountAvoid * avoidWeight) / (1f + avoidWeight);
            float finalTurnAmount = Mathf.Lerp(currentInput.x, desiredTurnAmount, maxTurnChange);

            // Reduce forwardAmount depending on turnAmount (to drive cleaner curves)
            float forwardReduction = Math.Max(1f - finalTurnAmount * finalTurnAmount, 0.3f);
            float finalForwardAmount = forwardReduction * forwardAmount;

            currentInput = new Vector2(finalTurnAmount, finalForwardAmount);
            OnMove?.Invoke(currentInput);
        }

        private void CollisionAvoidance(out float weight, out float angle)
        {
            float stepSize = maxRaycastDegree / nrRaycasts;
            weight = 0f;
            angle = 0f;

            float closestObstacle = float.MaxValue;

            float totalAngle = 0f;
            int angleNr = 0;

            for (int i = 0; i < nrRaycasts; i++)
            {
                float turnDeg = i * stepSize;

                for (int lr = -1; lr <= 1; lr += 2)
                {
                    float currentDeg = turnDeg * lr;

                    if (rb.velocity.z > 0f && currentDeg > 90f && currentDeg < 270f)
                        continue;

                    float additionalDeg = currentInput.x * MAX_STEERING_ANGLE;

                    Vector3 origin = transform.position + transform.forward * 1.5f;
                    Vector3 dir = Quaternion.Euler(0f, currentDeg + additionalDeg, 0f) * transform.forward;

                    Ray ray = new Ray(origin, dir);
                    RaycastHit hitInfo;

                    Color rayColor = Color.green;

                    if (Physics.Raycast(ray, out hitInfo, raycastDistance, detectionLayerMask))
                    {
                        if (useForceReverse && currentDeg < 15f && hitInfo.distance < forceReverseDist &&
                            rb.velocity.magnitude < 3f)
                        {
                            forceReverse = true;
                            StartCoroutine(ForceReverseHelper());
                            return;
                        }

                        /*
                        if (Vector3.Distance(transform.position, hitInfo.collider.ClosestPoint(transform.position)) < closestObstacle)
                        {
                            totalAngle = 0f;
                            angleNr = 0;
                        }
                        */

                        Vector3 hitVector = transform.position - hitInfo.transform.position;

                        angle = Vector3.SignedAngle(transform.forward, hitVector, Vector3.up);

                        // Closer obstacle = higher weight 
                        weight = 1f - hitInfo.distance / raycastDistance;

                        float currentAngle = Vector3.SignedAngle(transform.forward, hitVector, Vector3.up);
                        //currentAngle = currentAngle > 0f ? 90f - currentAngle : -90f + angle;
                        totalAngle += currentAngle * weight;
                        angleNr++;

                        rayColor = Color.red;
                    }

                    if (drawDebug)
                        Debug.DrawLine(ray.origin, ray.origin + ray.direction * raycastDistance, rayColor);
                }
            }

            float oldAngle = angle;
            angle = totalAngle / angleNr;
        }

        private void DespawnEnemy()
        {
            Debug.Log("ENEMY: Despawning " + gameObject.name);
            aiSpawner.SpawnEnemy();
            aiSpawner.UnregisterEnemy(gameObject);
            Destroy(gameObject);
        }

        private IEnumerator ForceReverseHelper()
        {
            yield return new WaitForSeconds(forceReverseDuration);

            forceReverse = false;
        }

        public void InvokeOnUse(bool use)
        {
            float invokeVal = use ? 1f : 0f;
            OnUse?.Invoke(invokeVal);
        }

        public void OnModuleAttached(CarModule carModule)
        {
            if (carModule.type == ModuleTypes.WEAPON || carModule.type == ModuleTypes.CANON)
                attackType = AttackType.Ranged;
        }

        public void SetCustomMaterials(Material main, Material tire)
        {
            foreach (MeshRenderer mesh in mainMaterialMeshes)
            {
                mesh.material = main;
            }

            foreach (MeshRenderer mesh in tireMaterialMeshes)
            {
                mesh.material = tire;
            }
        }

        public void OnZoneChanged(int newZone)
        {
            if(shouldDespawn)
                return;
            
            shouldDespawn = Math.Abs(spawnZone - newZone) > 1.0001f;
            if(shouldDespawn)
                Debug.Log("ENEMY: " + gameObject.name + " wants to despawn because he is in " + newZone + " but comes from " + spawnZone);
        }
    }
}