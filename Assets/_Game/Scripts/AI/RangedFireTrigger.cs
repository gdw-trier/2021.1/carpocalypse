using System;
using System.Collections;
using System.Collections.Generic;
using AI;
using UnityEngine;

namespace AI
{
    public class RangedFireTrigger : MonoBehaviour
    {
        [Tooltip("Updates every X FixedUpdates")]
        [SerializeField] private int updateInterval;

        [Tooltip("Should only have Player selected")]
        [SerializeField] private LayerMask playerDetectionMask;
        
        [SerializeField] private Vector3 boxCastExtents = new Vector3(1f, 1f, 1f);
        
        [SerializeField] private float attackRange = 100f;

        [SerializeField] private bool drawDebug;

        [Serializable]
        enum RangedType
        {
            None, MG, Grenade
        }

        [SerializeField] private RangedType type;

        private int _remainingCooldown = 0;
        private DynamicAI _dynamicAI;
        private bool _isFiring;

        private void Start()
        {
            _dynamicAI = transform.GetComponentInParent<DynamicAI>();
        }

        private void FixedUpdate()
        {
            if (--_remainingCooldown <= 0)
            {
                bool wasFiring = _isFiring;
                
                _isFiring = InRangeCheck();
                
                if(wasFiring != _isFiring)
                    _dynamicAI.InvokeOnUse(_isFiring);
                
                _remainingCooldown = updateInterval;
            }
        }

        private bool InRangeCheck()
        {
            Vector3 origin = transform.position;
            Vector3 direction = transform.parent.forward;
            
            if(drawDebug)
                DrawBoxCast(origin, boxCastExtents / 2f, direction, attackRange);
            
            return Physics.BoxCast(origin, boxCastExtents / 2f, direction, Quaternion.identity,
                attackRange, playerDetectionMask);
        }

        private void DrawBoxCast(Vector3 origin, Vector3 halfExtents, Vector3 direction, float attackRange)
        {
            for (int x = -1; x <= 1; x += 2)
            {
                for (int y = -1; y <= 1; y += 2)
                {
                    Vector3 start = new Vector3(origin.x, origin.y + halfExtents.y * y, origin.z + halfExtents.x * x);
                    Vector3 end = start + direction * attackRange;
                    
                    Debug.DrawLine(start, end, Color.red);
                }
            }
        }
    }
}
