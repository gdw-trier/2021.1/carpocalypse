using MapGeneration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnLoadRigid : MonoBehaviour
{
    private bool loadingComplete = false;
    Rigidbody rigid;
    public LayerMask terrainLayer;
    private void Start()
    {
        rigid = GetComponent<Rigidbody>();
        rigid.useGravity = false;
    }

    private void FixedUpdate()
    {
        if (BiomManager.current.worldGenerator.starting >0.99f && !loadingComplete)
        {
            if (Physics.Raycast(transform.position+ Vector3.up*200, Vector3.down, out RaycastHit hit, 1000, terrainLayer))
                transform.position = hit.point + Vector3.up * 2;
            
            rigid.useGravity = true;
            loadingComplete = true;
            
            if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("MainMenu"))
                SceneManager.UnloadSceneAsync("MainMenu");

            Destroy(this);
        }
    }

}
