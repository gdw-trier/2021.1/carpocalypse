using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class SetMouseSensetivty : MonoBehaviour
{
    private CinemachineFreeLook freeLook;
    private float startX;
    private float startY;

    private void Start()
    {
        freeLook = GetComponent<CinemachineFreeLook>();
        startY = freeLook.m_YAxis.m_MaxSpeed;
        startX = freeLook.m_XAxis.m_MaxSpeed;
    }

    void Update()
    {
        freeLook.m_YAxis.m_MaxSpeed = startY * Settings.sensitivity;
        freeLook.m_XAxis.m_MaxSpeed = startX * Settings.sensitivity;
    }
}
