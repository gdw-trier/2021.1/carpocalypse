using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScaler : MonoBehaviour
{
    private Vector3 localScaleRef;

    void Start()
    {
        localScaleRef = transform.localScale;
        StartCoroutine(DoScaling());
    }

    private IEnumerator DoScaling()
    {
        for (float i = 0; i < 1; i += Time.deltaTime * 2)
        {
            transform.localScale = localScaleRef * i;
            yield return new WaitForEndOfFrame();
        }
        transform.localScale = localScaleRef;
        Destroy(this);
    }
}
