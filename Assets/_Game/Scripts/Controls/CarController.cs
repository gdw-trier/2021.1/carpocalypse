using System;
using Controls;
using UnityEngine;

public class CarController : MonoBehaviour
{
    [Header("General")] [SerializeField] private float torque;
    [SerializeField] private float brakeTorque;
    [SerializeField] private float steeringAngle;
    [SerializeField] private float jumpForce;

    [Header("WheelColliders")] [SerializeField]
    private WheelCollider frontLeftCollider;

    [SerializeField] private WheelCollider frontRightCollider;
    [SerializeField] private WheelCollider backLeftCollider;
    [SerializeField] private WheelCollider backRightCollider;

    [Header("WheelModels")] [SerializeField]
    private Transform frontLeftModel;

    [SerializeField] private Transform frontRightModel;
    [SerializeField] private Transform backLeftModel;
    [SerializeField] private Transform backRightModel;

    [SerializeField] private Transform flDamper;
    [SerializeField] private Transform frDamper;
    [SerializeField] private Transform blDamper;
    [SerializeField] private Transform brDamper;

    [SerializeField] private Vector3 centerOfMass;
    [SerializeField] private float gravityMultiplier = 3;
    [SerializeField] private float maxVelocity = 30;
    [SerializeField] private LayerMask ground;
    [SerializeField] private float rollSpeed;
    [SerializeField] private float pitchSpeed;
    [SerializeField] private ParticleSystem landingVFX;
    [SerializeField] private AudioSource engineAudio;
    [SerializeField] private AudioClip[] engineSounds;

    private Rigidbody rb;
    private bool braking;
    private Vector2 input;
    private IInputProvider provider;

    private Quaternion frontLeftStartRotation;
    private Quaternion frontRightStartRotation;
    private Quaternion backLeftStartRotation;
    private Quaternion backRightStartRotation;
    private bool wasInAir;
    private int lastEngineIndex;


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        provider = GetComponent<IInputProvider>();

        ChangeInputProvider(provider);

        frontLeftStartRotation = frontLeftModel.localRotation;
        frontRightStartRotation = frontRightModel.localRotation;
        backLeftStartRotation = backLeftModel.localRotation;
        backRightStartRotation = backRightModel.localRotation;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void ChangeInputProvider(IInputProvider inputProvider)
    {
        inputProvider.OnMove += vector2 => input = vector2;
        inputProvider.OnBrake += f => braking = f > 0;
        inputProvider.OnSpecial += () =>
        {
            if (IsGrounded()) rb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
        };
        inputProvider.OnPause += () =>
        {
            GameManager.Instance.PauseGame();
        };
    }

    private void Start()
    {
        rb.centerOfMass = centerOfMass;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawWireSphere(transform.position + centerOfMass, 0.1f);
    }


    private void FixedUpdate()
    {
        rb.AddForce(Physics.gravity * (gravityMultiplier - 1));
        if (rb.velocity.magnitude > maxVelocity)
        {
            rb.velocity = rb.velocity.normalized * maxVelocity;
        }

        if (!IsGrounded())
        {
            InAir();
            wasInAir = true;
        }
        else if (wasInAir)
        {
            wasInAir = false;
            landingVFX.gameObject.SetActive(true);
            landingVFX.Play(true);
        }


        UpdateMotorTorque();
        UpdateBrake();
        UpdateSteering();


        UpdateModelPosition(frontLeftModel, frontLeftCollider, frontLeftStartRotation, flDamper);
        UpdateModelPosition(frontRightModel, frontRightCollider, frontRightStartRotation, frDamper);
        UpdateModelPosition(backLeftModel, backLeftCollider, backLeftStartRotation, blDamper);
        UpdateModelPosition(backRightModel, backRightCollider, backRightStartRotation, brDamper);
        if (IsGrounded())
            UpdateSound();
    }

    private void InAir()
    {
        float x = input.y * pitchSpeed + Time.fixedDeltaTime;
        float z = -input.x * rollSpeed + Time.fixedDeltaTime;

        Vector3 forward = transform.InverseTransformDirection(transform.forward);
        rb.MoveRotation(rb.rotation * Quaternion.AngleAxis(z, forward));
        Vector3 right = transform.InverseTransformDirection(transform.right);
        rb.MoveRotation(rb.rotation * Quaternion.AngleAxis(x, right));
    }

    private bool IsGrounded()
    {
        return IsGrounded(frontLeftModel) || IsGrounded(frontRightModel) || IsGrounded(backLeftModel) ||
               IsGrounded(backRightModel);
    }

    private bool IsGrounded(Transform t)
    {
        return Physics.Raycast(t.position, Vector3.down, 0.6f, ground);
    }

    private void UpdateMotorTorque()
    {
        frontLeftCollider.motorTorque = torque * input.y;
        frontRightCollider.motorTorque = torque * input.y;
        backLeftCollider.motorTorque = torque * input.y;
        backRightCollider.motorTorque = torque * input.y;
    }

    private void UpdateBrake()
    {
        float brakingForce = braking ? brakeTorque : 0;
        frontLeftCollider.brakeTorque = brakingForce;
        frontRightCollider.brakeTorque = brakingForce;
        backLeftCollider.brakeTorque = brakingForce;
        backRightCollider.brakeTorque = brakingForce;
    }

    private void UpdateSteering()
    {
        frontLeftCollider.steerAngle = steeringAngle * input.x;
        frontRightCollider.steerAngle = steeringAngle * input.x;
    }

    private void UpdateModelPosition(Transform model, WheelCollider collider, Quaternion startRotation,
        Transform damper)
    {
        collider.GetWorldPose(out Vector3 pos, out Quaternion rot);
        model.position = pos;
        if (damper != null) damper.position = pos;
        model.rotation = rot * startRotation;
    }

    private void UpdateSound()
    {
        if (engineSounds.Length < 4) return;
        if (rb.velocity.magnitude / maxVelocity < 0.25f || (Math.Abs(input.y) < 0.01f && !IsGrounded()))
        {
            if (lastEngineIndex == 0) return;
            lastEngineIndex = 0;
            engineAudio.clip = engineSounds[0];
            engineAudio.Play();
        }
        else if (rb.velocity.magnitude / maxVelocity < 0.5f)
        {
            if (lastEngineIndex == 1) return;
            lastEngineIndex = 1;
            engineAudio.clip = engineSounds[1];
            engineAudio.Play();
        }
        else if (rb.velocity.magnitude / maxVelocity < 0.75f)
        {
            if (lastEngineIndex == 2) return;
            lastEngineIndex = 2;
            engineAudio.clip = engineSounds[2];
            engineAudio.Play();
        }
        else if (rb.velocity.magnitude / maxVelocity < 1f)
        {
            if (lastEngineIndex == 3) return;
            lastEngineIndex = 3;
            engineAudio.clip = engineSounds[3];
            engineAudio.Play();
        }
    }
}