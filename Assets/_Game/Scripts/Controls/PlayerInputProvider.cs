using System;
using UnityEngine;

namespace Controls
{
    public class PlayerInputProvider : MonoBehaviour, IInputProvider
    {
        private ControlAsset input;

        public event Action<Vector2> OnMove;
        public event Action<Vector2> OnLook;
        public event Action<float> OnUse;
        public event Action<float> OnBrake;
        public event Action OnSpecial;

        public event Action OnPause;

        private void OnEnable()
        {
            input.Gameplay.Enable();
        }

        private void OnDisable()
        {
            input.Gameplay.Disable();
        }

        private void Awake()
        {
            input = new ControlAsset();
            input.Gameplay.Move.performed += context =>
            {
                if(!enabled) return;
                OnMove?.Invoke(context.ReadValue<Vector2>());
            };
            input.Gameplay.Look.performed += context =>
            {
                if(!enabled) return;
                OnLook?.Invoke(context.ReadValue<Vector2>());
            };
            input.Gameplay.Use.performed += context =>
            {
                if(!enabled) return;
                OnUse?.Invoke(1);
            };
            input.Gameplay.Break.performed += context =>
            {
                if(!enabled) return;
                OnBrake?.Invoke(1);
            };
            input.Gameplay.Use.canceled += context =>
            {
                if(!enabled) return;
                OnUse?.Invoke(0);
            };
            input.Gameplay.Break.canceled += context =>
            {
                if(!enabled) return;
                OnBrake?.Invoke(0);
            };

            input.Gameplay.Special.performed += context =>
            {
                if (!enabled) return;
                OnSpecial?.Invoke();
            };

            input.Gameplay.Pause.performed += context =>
            {
                if (!enabled) return;
                OnPause?.Invoke();
            };
        }
    }
}