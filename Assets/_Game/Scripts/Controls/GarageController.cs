using System;
using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;

public class GarageController : MonoBehaviour , IInputProvider
{

    [HideInInspector]public Rigidbody car;
    [SerializeField]private Transform target;
    [SerializeField] private float stoppingDistance;
    
    public event Action<Vector2> OnMove;
    public event Action<Vector2> OnLook;
    public event Action<float> OnUse;
    public event Action<float> OnBrake;
    public event Action OnSpecial;
    public event Action OnPause;

    public void FixedUpdate()
    {
        if(car == null) return;
        
        float forwardAmount;
        float turnAmountSeek;
        
        float distanceToTarget = Vector3.Distance(car.position, target.position);
        if (distanceToTarget > stoppingDistance)
        {
            Vector3 dirToMovePosition = (target.position - car.position).normalized;
            float dot = Vector3.Dot(car.transform.forward, dirToMovePosition);

            forwardAmount = dot > 0 ? 0.2f : -0.2f;

            float angleToDir = Vector3.SignedAngle(car.transform.forward, dirToMovePosition, Vector3.up);

            if (Mathf.Abs(angleToDir) < 90f)
                turnAmountSeek = angleToDir / 45f;
            else
            {
                turnAmountSeek = angleToDir > 0 ? 1f : -1f;
            }
            

            if (car.velocity.magnitude > 8)
            {
                car.velocity =  car.velocity * 0.8f;
                OnBrake?.Invoke(1);
                forwardAmount = 0;
            }
            OnMove?.Invoke(new Vector2(turnAmountSeek,forwardAmount));
            OnBrake?.Invoke(0);
        }
        else
        {
            OnMove?.Invoke(Vector2.zero);
            OnBrake?.Invoke(1);
            enabled = false;
        }
    }



}
