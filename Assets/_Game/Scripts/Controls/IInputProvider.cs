using System;
using UnityEngine;

namespace Controls
{
    public interface IInputProvider
    {
        public event Action<Vector2> OnMove;
        public event Action<Vector2> OnLook;

        public event Action<float> OnUse;

        public event Action<float> OnBrake;

        public event Action OnSpecial;

        public event Action OnPause;
    }
}
