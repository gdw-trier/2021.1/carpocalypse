using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Data/Highscore", fileName = "Highscore")]
[Serializable]
public class Highscore : ScriptableObject
{
    public int CurrentScore
    {
        get
        {
            return (int) totalDistanceReward + totalKillReward + other;
        }

        set
        {
            totalDistanceReward = value;
            totalKillReward = value;
            other = value;
        }
    }

    private float totalDistanceReward;

    public HighscoreData data;
    
    private int totalKillReward;
    private int other;
    public event Action<int> OnChange;
    public void UpdateDistance(float reward)
    {
        totalDistanceReward = Mathf.Max(totalDistanceReward, reward);
    }
    
    public void OnEnemyKilled(int killReward)
    {
        OnChange?.Invoke(killReward);
        totalKillReward += killReward;
    }

    public void OnOtherScore(int score)
    {
        OnChange?.Invoke(score);
        other += score;
    }

    public void ResetScore()
    {
        if (data.previousScores.Count < 3)
        {
            for (int i = data.previousScores.Count - 1; i < 3; i++)
            {
                data.previousScores.Add(Random.Range(0,2));
                data.previousNames.Add(RandomName.Get());
            }
        }
        data.previousScores.Add(CurrentScore);
        data.previousNames.Add(RandomName.Get());
        

        CurrentScore = 0;
    }
}

public class RandomName
{
    private static string[] names = new[]
    {
        "Rainer Zufall",
        "Lorad of Toast",
        "Backstage Basti",
        "Dosenbier Dagmar",
        "Dobby Duebelmann",
        "Nicole",
        "Shelly",
        "Flash Faultier",
        "Gustav Zahnbuerst",
        "Scherben Jesus",
        "Bubbele",
        "Scherben Jesus",
        "Manni",
        "Zecke",
        "Gitarrenbasti",
        "Synthiseb",
        "Grubby Ryna",
        "Sir Juliana Sweet"

    };
    
    public static string Get()
    {
        return names[Random.Range(0, names.Length)];
    }
}

public class HighscoreData
{
    public List<int> previousScores;
    public List<string> previousNames;

    public HighscoreData()
    {
        previousScores = new List<int>();
        previousNames = new List<string>();
    }
}
