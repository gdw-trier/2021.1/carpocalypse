using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Controls;
using UnityEngine;

public class Heal : MonoBehaviour
{
    public int amount;
    public ParticleSystem blood;
    public AudioSource src;
    public GameObject mesh1;
    public GameObject mesh2;
    public GameObject icon;
    bool triggered = false;
    private void OnCollisionEnter(Collision other)
    {
        PlayerInputProvider player = other.gameObject.GetComponentInParent<PlayerInputProvider>();
        if (!triggered && player)
        {
            player.GetComponent<Health>().Change(amount);
            if(blood) blood.Play();
            if(src) src.Play();
            mesh1.SetActive(false);
            mesh2.SetActive(false);
            icon.SetActive(false);
            Destroy(gameObject,0.5f);
            triggered = true;
        }
    }
}
