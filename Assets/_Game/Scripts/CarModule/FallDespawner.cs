using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDespawner : MonoBehaviour
{
    // Update is called once per frame
    void FixedUpdate()
    {
        if(transform.position.y < -20)
        {
            Destroy(gameObject);
        }
    }
}
