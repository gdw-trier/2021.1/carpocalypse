using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CarModuleSlot : MonoBehaviour
{
    public ModuleTypes moduleType;
    public Rigidbody carRigid;
    public CarModuleManager moduleManager;
    public Health carHealth;

    public event Action<CarModule> OnAttach;
    public event Action OnDetach;

    private void Awake()
    {
        carRigid = GetComponentInParent<Rigidbody>();
        moduleManager = GetComponentInParent<CarModuleManager>();
        carHealth = GetComponentInParent<Health>();
    }

    public bool ReplaceModule(CarModule module,bool destroy = false)
    {
        if (!module)
            return false;
        if (!module.CanTake())
            return false;

        CarModule myModule = GetModule();
        if (!myModule)
        {
            if (!module.OnAttach(this))
                return false;

            OnAttach?.Invoke(module);
            
            return true;
        }
        else
        {
            if (myModule == module)
                return true;
            if (myModule.value >= module.value)
                return false;
            if (!module.OnAttach(this))
                return false;


            myModule.OnBreak(destroy);

            OnAttach?.Invoke(module);

        }


        return true;
    }
    public bool IsEmpty()
    {
        return !GetModule();
    }


    public bool Detach(bool destroy = false, CarModule killModule = null)
    {
        CarModule module = GetModule();
        if (killModule && killModule != module)
            return false;
        if (!module || !module.gameObject || !module.transform)
            return false;

        module.OnDetach(destroy);
        OnDetach?.Invoke();
        return true;
    }
    public bool Wipe()
    {

        CarModule module = GetModule();
        if (!module || !module.gameObject || !module.transform)
            return false;

        module.OnDetach();
        Destroy(module.gameObject);
        return true;
    }

    public bool CanAddModule(CarModule carModule)
    {
        if (!carModule)
            return false;
        return IsEmpty() && carModule.CanTake() && carModule.type == moduleType;
    }

    public CarModule GetModule()
    {
        return GetComponentInChildren<CarModule>();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,0.5f);
        Gizmos.DrawRay(transform.position, transform.up * 0.5f);
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, transform.right * 0.5f);
    }
}
