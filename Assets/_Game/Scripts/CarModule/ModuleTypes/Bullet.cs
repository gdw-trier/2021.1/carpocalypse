using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [Header("Bullet")]
    public float speed = 10;
    [HideInInspector]public int damage = 10;
    public bool useExplosionDamage;
    public Data.TeamSetup teamSetup;
    public int bounces = 1;
    public float minSpeed = 5;
    Rigidbody rigid;
    public GameObject explosionPrefab;

    [Header("Seeking")]
    public bool useSeeking = false;
    public float seekingDelay = 1;
    public float seekingRadius;
    public float seekingForce = 300;
    Transform target;
    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        rigid.velocity = transform.forward * speed;
        Invoke("Kill", 10);
    }
    public void Kill()
    {
        CreateExplosion();
        Destroy(gameObject);
    }

    private void FixedUpdate()
    {



        transform.forward = rigid.velocity.normalized;

        if (rigid.velocity.magnitude < minSpeed)
            Kill();
        if(useSeeking)
            Seek();
    }

    public void Seek()
    {
        seekingDelay -= Time.fixedDeltaTime;

        if (!useSeeking || seekingDelay > 0)
            return;

        // Find Target
        if (!target && teamSetup)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, seekingRadius, teamSetup.enemyLayer);
            if (colliders.Length > 0)
            {
                target = colliders[0].transform;
                float closestDistance = Vector3.Distance(transform.position, target.position);
                foreach (Collider collider in colliders)
                {
                    float distance = Vector3.Distance(transform.position, collider.transform.position);
                    if (closestDistance < distance)
                    {
                        target = collider.transform;
                        closestDistance = distance;

                    }
                }
            }
        }

        if (target)
        {
            Vector3 dir = (target.position - transform.position).normalized;
            Debug.DrawRay(transform.position, dir, Color.red);
            rigid.AddForce(dir * seekingForce);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(out Health health))
        {
            Team team = teamSetup ? teamSetup.team : Team.NONE;
            if(!useExplosionDamage) health.Change(-damage, team);
            Kill();
            return;
        }
        if(bounces<=0)
            Kill();
        bounces--;
        //CreateExplosion();
    }

    public void CreateExplosion()
    {

        if (explosionPrefab)
        {
            GameObject expObj = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            if (expObj.TryGetComponent(out Explosion explosion))
            {
                explosion.team = teamSetup.team;
                explosion.damage = useExplosionDamage ? damage : 0;
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = seekingDelay <= 0 ? Color.red : Color.yellow;
        if (useSeeking)
            Gizmos.DrawWireSphere(transform.position, seekingRadius);
    }
}
