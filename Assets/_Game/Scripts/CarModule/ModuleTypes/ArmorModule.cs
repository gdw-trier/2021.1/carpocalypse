using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorModule : CarModule
{
    [Header("Armor")]
    public int health = 10;
    public GameObject[] moduleAlternatives;


    public override bool OnAttach(CarModuleSlot moduleSlot)
    {
        if (!moduleSlot)
            return false;

        if (base.OnAttach(moduleSlot))
        {
            if (moduleSlot && moduleSlot.carHealth)
            {
                moduleSlot.carHealth.maxHealth += health;
                moduleSlot.carHealth.health += health;
            }


            return true;
        }


        GameObject moduleAltPrefab = FindAlterantiveModule(moduleSlot);
        if (!moduleAltPrefab)
            return false;

        GameObject moduleAltObj = Instantiate(moduleAltPrefab);
        CarModule moduleAlt = moduleAltObj.GetComponent<CarModule>();
        moduleAlt.OnAttach(moduleSlot);
        moduleAlt.sfxAttach.Invoke("Play", 0.1f);

        Destroy(this.gameObject);

        print("Attached; " + moduleAltObj.name);

        return true;
    }

    public override void OnDetach(bool destroy = false)
    {

        if (moduleSlot && moduleSlot.carHealth)
        {
            moduleSlot.carHealth.maxHealth -= health;
        }

        base.OnDetach(destroy);
    }

    public GameObject FindAlterantiveModule(CarModuleSlot slot)
    {
        foreach(GameObject moduleAlt in moduleAlternatives)
        {
            if (moduleAlt.TryGetComponent(out CarModule module)) 
            {
                if (module.type == slot.moduleType)
                    return moduleAlt;
            }

        }
        return null;
    }
}
