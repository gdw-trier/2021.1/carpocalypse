using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeModule : CarModule
{
    [Header("Spike")]
    [SerializeField] private float minVelocity;
    [SerializeField] private float maxVelocity;
    [SerializeField] private int damage;
    [SerializeField] private AnimationCurve damageOverVelocity;
    [SerializeField] private float addedPushSpeed = 3f;

    private void OnTriggerEnter(Collider other)
    {
        if (!moduleSlot)
            return;
        Rigidbody otherRigid = other.gameObject.GetComponentInParent<Rigidbody>();
        if (otherRigid == moduleSlot.carRigid)
            return;
        if (otherRigid)
            otherRigid.velocity += moduleSlot.carRigid.velocity;

        float strength = moduleSlot.carRigid.velocity.magnitude;
        if (strength < minVelocity) return;
        Health health = other.gameObject.GetComponentInParent<Health>();
        if (health)
        {
            strength = Mathf.Min(strength, maxVelocity);
            int amount = Mathf.RoundToInt(damageOverVelocity.Evaluate(strength / maxVelocity) * damage);
            health.Change(-amount,moduleSlot.carHealth.teamSetup.team);
        }
    }
    

    public override bool OnAttach(CarModuleSlot moduleSlot)
    {
        if (!base.OnAttach(moduleSlot))
            return false;
        return true;

    }
}
