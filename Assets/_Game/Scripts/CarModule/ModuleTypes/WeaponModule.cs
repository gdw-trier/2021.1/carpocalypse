using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponModule : CarModule
{
    [Header("Weapon")]
    public Transform[] fireTransforms;
    int fireTransformIndex;
    public GameObject bulletPrefab;
    public float angleSpread;
    public float coolDown = 0;
    public int damage;
    float lastShot = 0;
    [Header("Aimassist")]
    public float aimAssistRadius = 3;
    public bool useAimAssist;
    
    static GameObject bulletParent;

    [SerializeField]private SFXPlayer sfxShoot;

    private void Start()
    {
        bulletParent = new GameObject("BulletParent");
    }

    private void FixedUpdate()
    {
        /*
        if (useAimAssist && moduleSlot && moduleSlot.carHealth)
        {
            Collider[] hits = Physics.OverlapSphere(transform.position, 30, moduleSlot.carHealth.teamSetup.enemyLayer);
            if (hits.Length == 0)
                return;

            Transform closest = hits[0].transform;
            foreach(Collider hit in hits)
            {
                if ((hit.transform.position - transform.position).magnitude < (closest.transform.position-transform.position).magnitude)
                    closest = hit.transform;
            }
            Vector3 otherDir = (closest.position - transform.position).normalized;
            otherDir.y = 0;
            Vector3 myDir = transform.parent.TransformDirection(Vector3.forward);
            myDir.y = 0;
            float angle = Vector3.Angle(myDir, otherDir);
            Vector3 dir = angle < 30 ? otherDir : myDir;
            transform.forward = dir;
        }*/
    }

    public override void OnUse()
    {
        if (Time.time - lastShot > coolDown)
            Shoot();
    }
    public override void OnUsing()
    {
        if (Time.time - lastShot > coolDown)
            Shoot();
    }

    public void Shoot()
    {
        if (!moduleSlot || !moduleSlot.carHealth)
            return;

        lastShot = Time.time;
        RaycastHit hit;
        sfxShoot.Play();

        Transform fireTransform = fireTransforms[fireTransformIndex];
        fireTransformIndex = (fireTransformIndex + 1) % fireTransforms.Length;

        Vector3 dir = fireTransform.forward;
        
        if (useAimAssist && Physics.SphereCast(fireTransform.position - dir*aimAssistRadius, aimAssistRadius, fireTransform.forward, out hit, 100, moduleSlot.carHealth.teamSetup.enemyLayer))
        {
            dir = (hit.point - fireTransform.position).normalized;
            //Debug.DrawLine(fireTransform.position, hit.point, Color.red);
            Debug.DrawRay(fireTransform.position, dir * 100, Color.red);
        }

        Quaternion rotation = Quaternion.LookRotation(dir);
        GameObject bulletObj = Instantiate(bulletPrefab, fireTransform.position, rotation, bulletParent.transform);


        if (moduleSlot && moduleSlot.carHealth && bulletObj.TryGetComponent(out Bullet bullet))
        {
            //print("Set Team for bullet");
            bullet.teamSetup = moduleSlot.carHealth.teamSetup;
            bullet.damage = damage;
        }

    }
}
