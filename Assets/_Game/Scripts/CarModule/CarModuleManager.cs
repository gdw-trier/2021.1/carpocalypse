using System.Collections;
using System.Collections.Generic;
using AI;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class CarModuleManager : MonoBehaviour
{
    public LayerMask moduleLayer;

    CarModuleSlot[] moduleSlots = new CarModuleSlot[0];
    CarModule[] modules = new CarModule[0];
    Controls.IInputProvider inputProvider;
    public Data.CarBlueprint blueprint;
    public bool usePickupReward = false;

    bool isUsing;

    public event Action<CarModule> OnAttach;
    public event Action OnDetach;
    public Health carHealth;

    private void Start()
    {
        moduleSlots = GetModuleSlots();
        inputProvider = GetComponent<Controls.IInputProvider>();
        inputProvider.OnUse += OnUse;
        carHealth = GetComponentInParent<Health>();



        if (carHealth)
            carHealth.OnDie += OnDie;
        if (blueprint)
            PopulateModules(blueprint.modules, true);
    }

    public void OnDie(GameObject go)
    {
        if (moduleSlots.Length == 0)
            moduleSlots = GetComponentsInChildren<CarModuleSlot>();

        CarModuleSlot[] slots = moduleSlots;
        for (int i = 0; i < slots.Length; i++)
        {
            var temp = slots[i];
            int randomIndex = UnityEngine.Random.Range(i, slots.Length);
            slots[i] = slots[randomIndex];
            slots[randomIndex] = temp;
        }

        foreach (CarModuleSlot slot in slots)
        {
            if (slot.Detach())
                return;
        }
    }


    private void FixedUpdate()
    {
        if (isUsing)
            OnUsing();
    }

    public void OnUsing()
    {
        foreach(CarModule module in modules)
        {
            module.OnUsing();
        }

    }
    public void OnUse(float n=0)
    {
        isUsing = n > 0;
        if (!isUsing)
            return;
        modules = GetCarModules();
        foreach(CarModule module in modules)
        {
            module.OnUse();
        }
    }

    public CarModule[] GetCarModules()
    {
        return GetComponentsInChildren<CarModule>();
    }
    public CarModuleSlot[] GetModuleSlots()
    {
        if (moduleSlots.Length == 0)
            moduleSlots = GetComponentsInChildren<CarModuleSlot>();

        foreach(CarModuleSlot slot in moduleSlots)
        {
            slot.OnAttach += OnAttach;
            slot.OnDetach += OnDetach;
        }

        return moduleSlots;
    }

    private void OnTriggerEnter(Collider other)
    {
        CarModule module = other.gameObject.GetComponentInParent<CarModule>();
        if (!module)
            return;
        bool couldAdd = AddCarModule(module);

        if (!couldAdd && !module.moduleSlot && Time.time - module.lastDrop > module.pickupDelay)
        {
            Destroy(module.gameObject);
        }
        if (usePickupReward && !module.moduleSlot)
        {
            GameManager.Instance.highscore.OnOtherScore(module.pickupReward);
        }
    }

    public bool AddCarModule(CarModule carModule, bool destroyOldModule = false)
    {
        CarModuleSlot[] slots = GetModuleSlots();
        foreach (CarModuleSlot slot in slots)
        {
            if (slot.IsEmpty())
                if (slot.ReplaceModule(carModule, destroyOldModule))
                    return true;
        }
        foreach (CarModuleSlot slot in slots)
        {
            if (slot.ReplaceModule(carModule))
                return true;
        }

        //GetComponent<DynamicAI>()?.OnModuleAttached(carModule);

        return false;

    }
    public bool CanAddModule(CarModule carModule)
    {
        CarModuleSlot[] slots = GetModuleSlots();
        foreach (CarModuleSlot slot in slots)
        {
            if (slot.CanAddModule(carModule))
                return true;
        }
        return false;
    }

    /// <summary>
    /// Populates Car with modules. Either with given prefabs or already instantiated objects
    /// </summary>
    /// <param name="moduleObjs"></param>
    /// <param name="isPrefab"></param>
    public void PopulateModules(GameObject[] moduleObjs, bool isPrefab = true, bool destroyOldModules = true)
    {
        if (destroyOldModules)
        {
            CarModuleSlot[] slots = GetModuleSlots();
            foreach (CarModuleSlot slot in slots)
            {
                slot.Wipe();
            }
        }
        foreach(GameObject go in moduleObjs)
        {
            if (!CanAddModule(go.GetComponent<CarModule>()))
                continue;

            GameObject moduleObj = isPrefab ? Instantiate(go) : go;
            CarModule module = moduleObj.GetComponent<CarModule>();
            AddCarModule(module,destroyOldModules);
        }
    }
}
