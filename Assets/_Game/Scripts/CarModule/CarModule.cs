using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public abstract class CarModule : MonoBehaviour
{
    [Header("Module")] public Rigidbody rigid;
    public ModuleTypes type;
    public int value = 1;
    public float detachSpeed = 10;
    public bool isStealable = false;
    public float pickupDelay = 1;
    public float lastDrop = -10;
    public int pickupReward = 10;


    public CarModuleSlot moduleSlot;
    Health moduleHealth;
    private InWorldUI inWorldUI;
    public bool addCarHealth = false;

    [SerializeField]public SFXPlayer sfxAttach;

    private void Awake()
    {
        lastDrop = -10;
        rigid = GetComponent<Rigidbody>();
        inWorldUI = GetComponentInChildren<InWorldUI>();
        TryGetComponent(out moduleHealth);

        SetRigid(moduleSlot==null);
    }


    public virtual void OnUse()
    {
    }

    public virtual void OnUsing()
    {
    }

    public virtual void OnBreak(bool destroy = false)
    {
        OnDetach(destroy);
        rigid.velocity = (rigid.centerOfMass).normalized * detachSpeed;
    }

    public virtual void OnDetach(bool destroy = false)
    {
        if (moduleSlot && moduleSlot.carHealth && moduleHealth && addCarHealth)
        {
            moduleHealth.parentHealth = null;
        }

        SetRigid(true);
        lastDrop = Time.time;
    }

    public void CheckDespawn()
    {
        if (!moduleSlot)
            Destroy(gameObject);
    }

    public virtual bool OnAttach(CarModuleSlot moduleSlot)
    {
        if (!moduleSlot)
            return false;
        if (moduleSlot.moduleType != type)
            return false;
        this.moduleSlot = moduleSlot;
        SetRigid(false);
        transform.SetParent(moduleSlot.transform);
        transform.rotation = moduleSlot.transform.rotation;
        transform.position = moduleSlot.transform.position;

        if (moduleHealth && moduleSlot.carHealth)
        {
            //print("SET teamSetup " + gameObject.name);
            moduleHealth.teamSetup = moduleSlot.carHealth.teamSetup;
            moduleHealth.parentHealth = moduleSlot.carHealth;
        }

        if (moduleSlot.carHealth && moduleHealth && addCarHealth)
        {
            //moduleSlot.carHealth.maxHealth += moduleHealth.maxHealth;
            //moduleSlot.carHealth.health += moduleHealth.health;
        }
        sfxAttach.Play();
        return true;
    }

    public bool CanTake()
    {
        if (moduleHealth && moduleHealth.GetPercentage() <= 0)
            return false;
        return (!moduleSlot || isStealable) && ((Time.time-lastDrop) > pickupDelay);
    }


    /// <summary>
    /// Setups the Rigidbody of the carmodule.
    /// false: "turn off" rigid
    /// true: "turn on" rigid
    /// </summary>
    /// <param name="flag"></param>
    public void SetRigid(bool flag)
    {

        if (!rigid)
            rigid = GetComponent<Rigidbody>();

        if (flag)
        {
            transform.parent = null;


            rigid.isKinematic = false;
            rigid.useGravity = true;

            CarModuleSlot oldSlot = moduleSlot;
            moduleSlot = null;
            if (oldSlot)
                oldSlot.Detach(false,this);

            inWorldUI.OnDetachFunc();

        }
        else
        {
            // Attached to Car
            rigid.isKinematic = true;
            rigid.useGravity = false;
            inWorldUI.OnAttachFunc();

        }
    }
}

public enum ModuleTypes
{
    WEAPON,
    SPIKE,
    CANON,
    ARMOR_FL,
    ARMOR_FR,
    ARMOR_RL,
    ARMOR_RR

}