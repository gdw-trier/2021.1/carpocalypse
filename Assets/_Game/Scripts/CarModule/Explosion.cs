using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] private AnimationCurve sizeOverTime;
    [SerializeField] private float durration = 1;
    [SerializeField] private float endSize = 10;
    [HideInInspector] public int damage = 10;
    public Team team = Team.NONE;

    private void Start()
    {
        StartCoroutine(Grow(durration,endSize));
    }

    // every 2 seconds perform the print()
    private IEnumerator Grow(float durration, float endSize)
    {
        float time =0;
        while (time < durration)
        {
            float percentage = time / durration;
            float size = sizeOverTime.Evaluate(percentage)*endSize;
            transform.localScale = Vector3.one * size;

            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        print("TRIGGER");
        Health health = other.GetComponentInParent<Health>();
        if (health)
        {
            print($"Explosion: damage {damage} team {team}");
            health.Change(-damage, team);
        }
    }
}
