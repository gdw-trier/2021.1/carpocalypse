using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Controls;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private bool saveToFile = false;
    [SerializeField] private string highscoreFilename = "highscore";
    [SerializeField] private float distanceReward = 1f;
    [SerializeField] private PauseMenu pauseMenu;
    public Highscore highscore;
    private float survivalIntervall;

    private Vector2 origin;
    
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    
    private string HighscoreFilePath 
    {
        get
        {
            return Path.Combine(Application.dataPath, highscoreFilename + ".json");
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(_instance.gameObject);
        }
        _instance = this;

        if (!player)
            player = FindObjectOfType<PlayerInputProvider>()?.transform;
    }

    private void Start()
    {
        LoadHighscore();
        ResetHighscore();

        if (player)
        {
            Vector3 playerPos = player.position;
            origin = new Vector2(playerPos.x, playerPos.z);
        }
    }

    void Update()
    {
        if(!player) return;
        Vector3 pos = player.position;
        float dist = Mathf.Sqrt(pos.x * pos.x + pos.z * pos.z);
        highscore.UpdateDistance(dist * distanceReward);
    }

    public void OnEnemyKilled(int killReward)
    {
        highscore.OnEnemyKilled(killReward);
    }

    public void ResetHighscore()
    {
        highscore.ResetScore();
        SaveHighscore();
    }

    private void LoadHighscore ()
    {
        Debug.Log(HighscoreFilePath);
        if (!saveToFile || !File.Exists(HighscoreFilePath))
        {
            highscore.data = new HighscoreData();
            return;
        }

        
        string jsonString = File.ReadAllText(HighscoreFilePath);
        highscore.data = JsonUtility.FromJson<HighscoreData>(jsonString);
    }

    private void SaveHighscore()
    {
        if(!saveToFile)
            return;
        
        string json = JsonUtility.ToJson(highscore.data);
        File.WriteAllText(HighscoreFilePath, json);
    }

    public void PauseGame()
    {
        if (!pauseMenu)
            pauseMenu = FindObjectOfType<PauseMenu>();

        pauseMenu.PauseGame();
    }

    public void OnGameLoadComplete()
    {
        Debug.Log(SceneManager.GetActiveScene());
        if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("MainMenu"))
            SceneManager.UnloadSceneAsync("MainMenu");
    }
}
