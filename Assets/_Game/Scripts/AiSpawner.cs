using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AI;
using Controls;
using Data;
using UnityEngine;
using Random = UnityEngine.Random;

public class AiSpawner : MonoBehaviour
{
    // Relevant for spawning
    [SerializeField] public GameObject enemyPrefab;
    [SerializeField] private Transform player;
    [SerializeField] private float minDistFromPlayer;
    [SerializeField] private int maxDistFromPlayer;
    [SerializeField] private float destroyDistance;
    [SerializeField] private float spawnHeight;
    [SerializeField] private float randomSpawnInterval = 30f;
    [SerializeField] private float spawnDelay = 5f;

    public float zoneSize = 1000f;

    // Relevant for balancing
    public List<CarBlueprint> enemyTypes;

    public List<EnemyZone> enemyZones;

    private Dictionary<GameObject, DynamicAI> cars;
    private float spawnTimer;

    private int activeZone = -1;
    private List<int> zoneSpawnIndex;

    [Header("Textures")] [SerializeField] private List<Material> carMaterials;
    [SerializeField] private List<Material> tireMaterials;

    public int Zone
    {
        get
        {
            Vector3 pos = player.position;
            float dist = Mathf.Sqrt(pos.x * pos.x + pos.z * pos.z);
            int result = 0;

            for (int i = 1; i < enemyZones.Count; i++)
            {
                if (dist < enemyZones[i].startingDistance)
                    break;

                result++;
            }

            return result;
        }
    }

    private int debugZone;

    private void Start()
    {
        cars = new Dictionary<GameObject, DynamicAI>();

        spawnTimer = randomSpawnInterval;

        if (!player)
            player = FindObjectOfType<PlayerInputProvider>().transform;

        zoneSpawnIndex = new List<int>();
        for (int i = 0; i < enemyZones.Count; i++)
        {
            zoneSpawnIndex.Add(-1);
        }
    }

    private void Update()
    {
        debugZone = Zone;

        if (spawnDelay > 0f)
        {
            spawnDelay -= Time.deltaTime;
            return;
        }

        foreach (GameObject car in cars.Keys)
        {
            if (Vector3.Distance(car.transform.position, player.transform.position) > destroyDistance)
            {
                car.transform.position = RandomPositionNearPlayer();
            }
        }

        // If zone changed
        if (activeZone != Zone)
        {
            activeZone = Zone;
            List<DynamicAI> enemies = cars.Values.ToList();
            foreach (var enemy in enemies)
            {
                enemy.OnZoneChanged(Zone);
            }
        }

        // Spawn enemies if there are not enough
        if (cars.Count < enemyZones[Zone].minEnemyCount)
        {
            for (int i = cars.Count; i < enemyZones[Zone].minEnemyCount; i++)
            {
                SpawnEnemy();
            }
        }

        // Randomly spawn enemies every randomSpawnInterval seconds
        spawnTimer -= Time.deltaTime;
        if (cars.Count < enemyZones[Zone].maxEnemyCount && spawnTimer <= 0f)
        {
            SpawnEnemy();
            spawnTimer = randomSpawnInterval * Random.Range(0.8f, 1.2f);
        }
    }

    public void InitialSpawn()
    {
        int spawnedEnemies = Random.Range(enemyZones[Zone].minEnemyCount, enemyZones[Zone].maxEnemyCount);

        for (int i = 0; i < spawnedEnemies; i++)
        {
            SpawnEnemy();
        }
    }

    public void SpawnEnemy()
    {
        Vector3 spawnPos = RandomPositionNearPlayer();

        Ray ray = new Ray(spawnPos, Vector3.down);
        RaycastHit hitInfo;

        // Avoids spawning on garage
        while (Physics.BoxCast(ray.origin, new Vector3(2f, 1f, 3.5f), ray.direction, Quaternion.identity,
            float.MaxValue, LayerMask.GetMask("Garage")))
        {
            spawnPos = RandomPositionNearPlayer();
        }

        // Tries to spawn near ground
        if (Physics.Raycast(ray, out hitInfo, float.MaxValue, LayerMask.GetMask("Terrain")))
        {
            spawnPos = hitInfo.point + Vector3.up * 5f;
        }

        GameObject car = Instantiate(enemyPrefab, spawnPos, Quaternion.identity, transform);

        CarBlueprint enemyBP = GetEnemyBP(enemyZones[Zone].spawnedEnemies);
        car.name = enemyBP.name;

        GameObject[] modules = enemyBP.modules;

        CarModuleManager moduleManager = car.GetComponent<CarModuleManager>();
        if (modules.Length > 0)
            moduleManager.PopulateModules(modules);

        moduleManager.blueprint = enemyBP;

        car.GetComponent<Health>().OnDie += go =>
        {
            cars.Remove(go);
            Destroy(go);
        };
        
        DynamicAI ai = car.GetComponent<DynamicAI>();
        ai.aiSpawner = this;
        ai.spawnZone = Zone;
        ai.SetCustomMaterials(RandomMat(carMaterials), RandomMat(tireMaterials));

        cars.Add(car, ai);
    }

    private Vector3 RandomPositionNearPlayer()
    {
        Vector2 dir2D = Random.insideUnitCircle;
        Vector3 dir = new Vector3(dir2D.x, 0, dir2D.y);
        Vector3 pos = player.transform.position + dir * Random.Range(minDistFromPlayer, maxDistFromPlayer);
        pos.y = spawnHeight;
        return pos;
    }

    private Material RandomMat(List<Material> list)
    {
        if (list.Count == 0)
        {
            Debug.LogError("EnemySpawner has missing materials");
            return null;
        }

        return list[Random.Range(0, list.Count)];
    }

    private CarBlueprint GetEnemyBP(List<CarBlueprint> list)
    {
        int index = (zoneSpawnIndex[Zone] + 1) % list.Count;
        zoneSpawnIndex[Zone] = index;
        return list[index];
    }

    public int GetEnemiesAlive()
    {
        return cars.Count;
    }

    public void UnregisterEnemy(GameObject enemy)
    {
        cars.Remove(enemy);
    }
}