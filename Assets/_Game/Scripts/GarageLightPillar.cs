using System.Collections;
using System.Collections.Generic;
using Controls;
using UnityEngine;

public class GarageLightPillar : MonoBehaviour
{
    [SerializeField] private LineRenderer pillarRenderer;
    [SerializeField] private Transform player;

    [SerializeField] private float lightPillarDistance;
    [SerializeField] private float lightMaxPillarDistance;

    // Start is called before the first frame update
    void Start()
    {
        if (!player)
            player = FindObjectOfType<PlayerInputProvider>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = player.position;
        float distance = (player.transform.position - transform.position).magnitude;

        pillarRenderer.enabled = (distance > lightPillarDistance && distance < lightMaxPillarDistance);
    }
}