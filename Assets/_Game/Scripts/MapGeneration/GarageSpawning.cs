using MapGeneration.Bioms;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarageSpawning : MonoBehaviour
{
    public StreetBiom streetBiom;
    private void OnEnable()
    {
        
    }

    private void OnDisable()
    {

        Vector2 position = (new Vector2(transform.position.x + (streetBiom.streetWidth) * 250, transform.position.z+(streetBiom.streetWidth) * 250)) / streetBiom.streetWidth;
        Vector2Int vector2Int = new Vector2Int((int)position.x, (int)position.y);

        streetBiom.spawnedGarages.Remove(vector2Int);
    }
}
