using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGeneration
{
    public class BiomManager : MonoBehaviour
    {
        [SerializeField] public List<ABiom> bioms;
        public WorldGenerator worldGenerator;

        public static BiomManager current;

        private new Renderer renderer;

        private void Awake()
        {
            renderer = worldGenerator.chunkPrefab.GetComponent<Renderer>();
            current = this;

            foreach (ABiom biom in bioms){
                biom.Init();
            }
        }
        public int GetMaterialIndex(Vector2 position, float correctHeight)
        {
            int maxMaterial = 0;
            float maxValue = 0;
            foreach (ABiom biom in bioms)
            {
                float biomValue = biom.IsBiom(position);
                if (biomValue > maxValue)
                {
                    maxValue = biomValue;
                    maxMaterial = biom.GetMaterial(position, correctHeight);
                }
            }
            return maxMaterial;
        }
        public Material GetMaterial(Vector2 position)
        {
            int tmp = GetMaterialIndex(position, GetHeight(position));
            return renderer.sharedMaterials[tmp];
        }

        public float GetHeight(Vector2 position)
        {
            float currentWeight = 0;
            List<Vector2> entries = new List<Vector2>();

            foreach (ABiom biom in bioms)
            {
                float biomValue = biom.IsBiom(position);
                if (biomValue > 0)
                {
                    biomValue = Mathf.Min(biomValue, 1- currentWeight);
                    currentWeight += biomValue;
                    entries.Add(new Vector2(biomValue, biom.GetHeight(position)));
                }
                if(currentWeight >= 0.999f)
                {
                    break;
                }
            }
            float height = 0;
            foreach (Vector2 v in entries)
            {
                height += (v.x / currentWeight) * v.y;
            }
            if (entries.Count == 0)
                height = 1337;
            return height;
        }

        public bool IsChunkLoaded(Vector2 position)
        {
            position /= Chunk.chunkSize;
            position = new Vector2((int)position.x, (int)position.y);
            position *= Chunk.chunkSize;
            return worldGenerator.chunkDict.ContainsKey(position);
        }

        public string GetRegionNameExcludingRoads(Vector2 position)
        {
            int maxBiom = 0;
            float maxValue = 0;

            for(int i=1; i<bioms.Count; i++)
            {
                float biomValue = bioms[i].IsBiom(position);
                if (biomValue > maxValue)
                {
                    maxValue = biomValue;
                    maxBiom = i;
                }
            }
            return bioms[maxBiom].name;
        }

        public int GetDominantBiomIndex(Vector2 position)
        {
            int maxBiom = 0;
            float maxValue = 0;

            for (int i = 0; i < bioms.Count; i++)
            {
                float biomValue = bioms[i].IsBiom(position);
                if (biomValue > maxValue)
                {
                    maxValue = biomValue;
                    maxBiom = i;
                }
            }
            return maxBiom;
        }

    }
}