using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace MapGeneration
{

    public class WorldGenerator : MonoBehaviour
    {
         public float starting = 0;

        [Header("Values that should be set initialy")]
        public GameObject chunkPrefab;
        public BiomManager biomManager;

        [HideInInspector]public Dictionary<Vector2, Chunk> chunkDict = new Dictionary<Vector2, Chunk>();
        private List<Chunk> polledChunks = new List<Chunk>();

        private ConcurrentQueue<Chunk> enqueuedChunks = new ConcurrentQueue<Chunk>();
        private ConcurrentQueue<Chunk> generatedChunks = new ConcurrentQueue<Chunk>();

        public void LoadChunk(Vector2 position)
        {
            Chunk newChunk;
            if(polledChunks.Count > 0)
            {
                newChunk = polledChunks[0];
                polledChunks.RemoveAt(0);
            }
            else
            {
                newChunk = GameObject.Instantiate(chunkPrefab,transform).GetComponent<Chunk>();
            }

            newChunk.InitChunk((int)position.x, (int)position.y);
            newChunk.transform.localPosition = new Vector3(position.x, 0, position.y);
            enqueuedChunks.Enqueue(newChunk);
        }

        public void UnloadChunk(Vector2 position)
        {
            if (chunkDict.ContainsKey(position))
            {
                polledChunks.Add(chunkDict[position]);
                chunkDict[position].gameObject.SetActive(false);
                chunkDict.Remove(position);
            }          
        }

        private void Start()
        {
            for(int i = 0; i < Environment.ProcessorCount/2-1; i++)
            {
                Thread worker = new Thread(WorkThread);
                worker.Priority = System.Threading.ThreadPriority.Highest;
                worker.Start();
            }
        }
        private void Update()
        {
            Chunk newChunk;
            while(generatedChunks.TryDequeue(out newChunk))
            {
                newChunk.ApplyChunk();
                if (chunkDict.ContainsKey(new Vector2(newChunk.xPosition, newChunk.zPosition)))
                {
                    Destroy(newChunk.gameObject);
                    continue;
                }
                chunkDict.Add(new Vector2(newChunk.xPosition,newChunk.zPosition), newChunk);
                newChunk.gameObject.SetActive(true);
            }

            float max = 0;
            switch (Settings.currentSetting)
            {
                case Settings.PerformanceSettings.Low:
                    max = 25;
                    break;
                case Settings.PerformanceSettings.Medium:
                    max = 49;
                    break;
                case Settings.PerformanceSettings.High:
                    max = 121;
                    break;
                case Settings.PerformanceSettings.Ultra:
                    max = 225;
                    break;
            }

            if (starting < 1)
            {
                starting = chunkDict.Count/ max;
            }
        }

        private void WorkThread()
        {
            while (true)
            {
                Chunk newChunk;
                while (enqueuedChunks.TryDequeue(out newChunk))
                {
                    newChunk.GenerateChunk();
                    generatedChunks.Enqueue(newChunk);
                }
                Thread.Sleep(1);
            }
        }
    }
}