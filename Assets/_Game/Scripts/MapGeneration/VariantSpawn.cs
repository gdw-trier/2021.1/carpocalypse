using System.Collections.Generic;
using UnityEngine;

public class VariantSpawn : MonoBehaviour
{
    public List<GameObject> prefabs;

    private void OnEnable()
    {
        int prefabIndex = (int)Random.Range(0, prefabs.Count - 0.001f);
        GameObject child = GameObject.Instantiate(prefabs[prefabIndex], transform.position, Quaternion.Euler(0, Random.Range(0, 360), 0),transform);
    }

    private void OnDisable()
    {
        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }
    }
}
