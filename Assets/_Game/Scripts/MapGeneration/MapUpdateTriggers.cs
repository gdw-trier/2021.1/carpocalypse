using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGeneration
{
    public class MapUpdateTriggers : MonoBehaviour
    {
        #region Variables
        [Header("Values that should be set initialy")]
        public Transform traggedObject;
        public WorldGenerator worldGenerator;
        [Range(50, 750)] public int loadingRange;
        [Range(100, 1000)] public int unloadingRange;

        private Vector2 currentChunkThird;
        private List<Vector2> loadedChunks = new List<Vector2>();
        private List<Vector2> startChunks = new List<Vector2>();

        bool isStarting = true;
        #endregion

        #region Unity called functions
        private void Start()
        {
            switch (Settings.currentSetting)
            {
                case Settings.PerformanceSettings.Low:
                    loadingRange = 300;
                    unloadingRange = 350;
                    break;
                case Settings.PerformanceSettings.Medium:
                    loadingRange = 400;
                    unloadingRange = 550;
                    break;
                case Settings.PerformanceSettings.High:
                    loadingRange = 600;
                    unloadingRange = 700;
                    break;
                case Settings.PerformanceSettings.Ultra:
                    loadingRange = 800;
                    unloadingRange = 900;
                    break;
            }

            //TODO!!! SET LOADING RANGE
            for (int x= -loadingRange+100; x <= loadingRange-100; x+=100)
            {
                for (int y = -loadingRange+100; y <= loadingRange-100; y+=100)
                {
                    loadedChunks.Add(new Vector2(x,y));
                    worldGenerator.LoadChunk(new Vector2(x, y));
                }
            }
            CheckLoadChunks();
        }

        private void Update()
        {
            Vector2 newChunkThird = new Vector2((int)(traggedObject.position.x / (Chunk.chunkSize / 3)), (int)(traggedObject.position.z / (Chunk.chunkSize / 3)));
            if(newChunkThird.x != currentChunkThird.x)
            {
                if((newChunkThird.x+9999) % 3 == 1)
                {
                    CheckUnloadChunks();
                }
                else
                {
                    CheckLoadChunks();
                }
                currentChunkThird.x = newChunkThird.x;
            }
            if (newChunkThird.y != currentChunkThird.y)
            {
                if ((newChunkThird.y + 9999) % 3 == 1)
                {
                    CheckUnloadChunks();
                }
                else
                {
                    CheckLoadChunks();
                }
                currentChunkThird.y = newChunkThird.y;
            }
        }

        private void CheckLoadChunks()
        {
            for(int xCurrent = Mathf.RoundToInt((traggedObject.transform.position.x- 1.5f * loadingRange) / Chunk.chunkSize) * Chunk.chunkSize, xMax = Mathf.RoundToInt((traggedObject.transform.position.x + 1.5f * loadingRange) / Chunk.chunkSize) * Chunk.chunkSize; xCurrent < xMax; xCurrent += Chunk.chunkSize)
            {
                for (int yCurrent = Mathf.RoundToInt((traggedObject.transform.position.z - 1.5f * loadingRange) / Chunk.chunkSize) * Chunk.chunkSize, yMax = Mathf.RoundToInt((traggedObject.transform.position.z + 1.5f * loadingRange) / Chunk.chunkSize) * Chunk.chunkSize; yCurrent < yMax; yCurrent += Chunk.chunkSize)
                {
                    if(Vector2.SqrMagnitude(new Vector2(xCurrent,yCurrent) - new Vector2(traggedObject.position.x, traggedObject.position.z)) < loadingRange * loadingRange)
                    {
                        if(!loadedChunks.Contains(new Vector2(xCurrent, yCurrent)))
                        {
                            loadedChunks.Add(new Vector2(xCurrent, yCurrent));
                            worldGenerator.LoadChunk(new Vector2(xCurrent, yCurrent));
                        }
                    }
                }
            }
        }
        private void CheckUnloadChunks()
        {
            for(int i=0; i< loadedChunks.Count;)
            {
                Vector2 currentVector2 = loadedChunks[i];
                if (Vector2.SqrMagnitude(currentVector2 - new Vector2(traggedObject.position.x, traggedObject.position.z)) > unloadingRange * unloadingRange)
                {
                    if (loadedChunks.Contains(currentVector2))
                    {
                        loadedChunks.Remove(currentVector2);
                        worldGenerator.UnloadChunk(currentVector2);
                        continue;
                    }
                }
                i++;
            }          
        }
        #endregion
    }
}