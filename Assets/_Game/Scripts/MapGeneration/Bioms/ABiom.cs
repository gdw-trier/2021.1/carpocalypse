using MapGeneration.Bioms;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGeneration
{
    [Serializable]
    public abstract class ABiom: ScriptableObject
    {
        public List<ProbFunction> probFunctions;
        public virtual void Init()
        {

        }
        public abstract float IsBiom(Vector2 position);
        public abstract int GetMaterial(Vector2 position, float correctHeight);
        public abstract float GetHeight(Vector2 position);
    }
}