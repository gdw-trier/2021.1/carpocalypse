using MapGeneration;
using MapGeneration.Bioms;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGeneration.Bioms
{
    [CreateAssetMenu(menuName = "Data/Bioms/Street")]
    public class StreetBiom : ABiom
    {
        #region variables
        public NoiseFunction heightFunction;
        public float garageProbabilityPerSegment;

        public bool doStripes;
        [HideInInspector] public List<Vector2Int> garagePositions = new List<Vector2Int>();
        public List<Vector2Int> spawnedGarages;

        public GameObject lightPillarPrefab;
        public GameObject garagePrefab;
        #endregion

        enum Direction { North, East, South, West };
        [HideInInspector]public int streetWidth = 10;

        private bool[,] streetFlagMap;

        public override void Init()
        {
            garagePositions.Clear();
            spawnedGarages.Clear();

            streetFlagMap = new bool[500, 500]; //optimized for distance between points = 5;

            SpawnGarage(new Vector2Int(250, 250));
            DrawLine(new Vector2Int(235, 250), 30, Direction.East);
            DrawLine(new Vector2Int(250, 235), 30, Direction.North);

            List<Vector2Int> streetPositions = new List<Vector2Int>(new Vector2Int[] { new Vector2Int(235, 250), new Vector2Int(250, 235) });
            int endReached = 0;
            int duped = 0;

            for (int i = 0; i < 1000 && endReached < 3; i++)
            {
                Vector2Int start = streetPositions[0];
                streetPositions.RemoveAt(0);

                float[] weights = new float[] { 0.05f, 0.05f, 0.05f };

                Vector2Int tmp = new Vector2Int(start.x - 250, start.y - 250);
                if (tmp.y > 0)
                    weights[0] += 0.25f;
                else
                    weights[2] += 0.25f;
                if (tmp.x > 0)
                    weights[1] += 0.25f;

                if (Mathf.Abs(tmp.x) > Mathf.Abs(tmp.y))
                {
                    weights[0] += 0.175f;
                    weights[2] += 0.175f;
                }
                else
                    weights[1] += 0.175f;

                weights[1] += weights[0];
                weights[2] += weights[1];

                int j = 0;
                float value = Random.value;
                do
                {
                    if (value < weights[j])
                        break;
                    j++;
                } while (j < 3);

                Vector2Int end = DrawLine(start, (int)(Random.Range(3, 15)) * 2, (Direction)j);
                if (end != new Vector2Int(-1, -1))
                {
                    streetPositions.Add(end);
                    if (Random.value < (end.magnitude) / 10000)
                    {
                        streetPositions.Add(end);
                        duped++;
                    }
                }
                else
                {
                    streetPositions.Add(new Vector2Int(250, 250));
                    endReached++;
                }
            }
        }

        public override float GetHeight(Vector2 position)
        {
            return heightFunction.GetValue(position);
        }

        public override int GetMaterial(Vector2 position, float height)
        {
            Vector2 streetPosition = (position + new Vector2(streetWidth * 250, streetWidth * 250)) / streetWidth;
            Vector2Int streetPositionInt = new Vector2Int((int)streetPosition.x, (int)streetPosition.y);

            if (doStripes)
            {
                if ((int)((position.x + streetWidth * 250) / 5) % 2 == 0 && (int)((position.y + streetWidth * 250) / 1) % 10 == 5 && streetPositionInt.x > 0 && streetPositionInt.x < 499 && (streetFlagMap[streetPositionInt.x - 1, streetPositionInt.y] || streetFlagMap[streetPositionInt.x + 1, streetPositionInt.y]))
                {
                    return 2;
                }
                if ((int)((position.y + streetWidth * 250) / 5) % 2 == 0 && (int)((position.x + streetWidth * 250) / 1) % 10 == 5 && streetPositionInt.y > 0 && streetPositionInt.y < 499 && (streetFlagMap[streetPositionInt.x, streetPositionInt.y - 1] || streetFlagMap[streetPositionInt.x, streetPositionInt.y + 1]))
                {
                    return 2;
                }
            }
            return 1;
        }

        public override float IsBiom(Vector2 position)
        {
            position = (position + new Vector2(streetWidth * 250, streetWidth * 250)) / streetWidth;
            Vector2Int vector2Int = new Vector2Int((int)position.x, (int)position.y);
            if (vector2Int.x < 1 || vector2Int.x >= 499 || vector2Int.y < 1 || vector2Int.y >= 499)
                return 0;
            if (streetFlagMap[vector2Int.x, vector2Int.y])
                return 1;

            float weight = 0;
            Vector2 difPos = position - vector2Int;
            if (streetFlagMap[vector2Int.x - 1, vector2Int.y])
                weight = Mathf.Max(weight, 1 - difPos.x);
            if (streetFlagMap[vector2Int.x + 1, vector2Int.y])
                weight = Mathf.Max(weight, difPos.x);

            if (streetFlagMap[vector2Int.x, vector2Int.y - 1])
                weight = Mathf.Max(weight, 1 - difPos.y);
            if (streetFlagMap[vector2Int.x, vector2Int.y + 1])
                weight = Mathf.Max(weight, difPos.y);

            if (streetFlagMap[vector2Int.x - 1, vector2Int.y - 1])
                weight = Mathf.Max(weight, 1 - difPos.magnitude);
            if (streetFlagMap[vector2Int.x - 1, vector2Int.y + 1])
                weight = Mathf.Max(weight, 1 - new Vector2(difPos.x, 1 - difPos.y).magnitude);

            if (streetFlagMap[vector2Int.x + 1, vector2Int.y - 1])
                weight = Mathf.Max(weight, 1 - new Vector2(1 - difPos.x, difPos.y).magnitude);
            if (streetFlagMap[vector2Int.x + 1, vector2Int.y + 1])
                weight = Mathf.Max(weight, 1 - new Vector2(1 - difPos.x, 1 - difPos.y).magnitude);

            return weight;
        }

        private Vector2Int DrawLine(Vector2Int startPos, int length, Direction dir)
        {
            streetFlagMap[startPos.x, startPos.y] = true;
            while (length > 0)
            {
                if (startPos.x < 1 || startPos.x >= 498 || startPos.y < 1 || startPos.y >= 498)
                    return new Vector2Int(-1, -1);
                if (!streetFlagMap[startPos.x, startPos.y])
                {
                    if(Random.Range(0,100)< garageProbabilityPerSegment)
                    {
                        SpawnGarage(startPos);
                    }
                }
                streetFlagMap[startPos.x, startPos.y] = true;
                length--;
                switch (dir)
                {
                    case Direction.North:
                        startPos.y++;
                        break;
                    case Direction.East:
                        startPos.x++;
                        break;
                    case Direction.South:
                        startPos.y--;
                        break;
                    case Direction.West:
                        startPos.x--;
                        break;
                }
            }
            return startPos;
        }
        private void SpawnGarage(Vector2Int position)
        {
            if (PointTaken(position))
            {
                return;
            }
            garagePositions.Add(position);

            streetFlagMap[position.x-1, position.y-1] = true;
            streetFlagMap[position.x-1, position.y] = true;
            streetFlagMap[position.x-1, position.y+1] = true;

            streetFlagMap[position.x, position.y - 1] = true;
            streetFlagMap[position.x, position.y] = true;
            streetFlagMap[position.x, position.y + 1] = true;

            streetFlagMap[position.x + 1, position.y - 1] = true;
            streetFlagMap[position.x + 1, position.y] = true;
            streetFlagMap[position.x + 1, position.y + 1] = true;

            Vector2 pos = position * streetWidth - new Vector2(streetWidth * 250, streetWidth * 250);
            Vector3 pos3 = new Vector3(pos.x, 0, pos.y);
                       
            GameObject.Instantiate(lightPillarPrefab,new Vector3(pos3.x,0, pos3.z),Quaternion.identity, BiomManager.current.worldGenerator.transform);
        }

        private bool PointTaken(Vector2Int pos)
        {
            foreach (Vector2Int v in garagePositions)
            {
                if (Vector2Int.Distance(v, pos) < 50) return true;
            }

            return false;
        }
    }
}