using System.Collections.Generic;
using UnityEngine;

namespace MapGeneration.Bioms
{
    [CreateAssetMenu(menuName = "Data/Condition/GroundType")]
    public class ProbGroundCondition : AProbApplyCondition
    {
        public List<int> allowedGroundMaterials;

        public bool Apply(int material)
        {
            return allowedGroundMaterials.Contains(material);
        }
    }
}