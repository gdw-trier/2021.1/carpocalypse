using UnityEngine;

namespace MapGeneration.Bioms
{
    [CreateAssetMenu(menuName = "Data/Condition/Noise")]

    public class NoiseCondition : AHeightApplyCondition
    {
        public enum Operators { Greater, Less }
        [Range(3, 200)] public float periode;
        [Range(0, 1f)] public float roughness;
        [Space]
        public float value_map_0;
        public float value_map_1;

        public override float Apply(Vector2 point)
        {
            float height = Mathf.PerlinNoise(10000 + point.x / periode, 10000 + point.y / periode);

            if (roughness > 0)
            {
                System.Random random = new System.Random((int)(point.x * 2 + point.y * 13907));
                height += (random.Next((int)(-roughness * 100), (int)(roughness * 100)) / 100f);
            }

            if(value_map_0< value_map_1)
            {
                return Mathf.Clamp01((height - value_map_0) / (value_map_1 - value_map_0));
            }
            else
            {
                return Mathf.Clamp01(1-((height - value_map_0) / (value_map_1 - value_map_0)));
            }
            return 0;
        }
    }
}