using System;
using UnityEngine;

namespace MapGeneration.Bioms
{
    [Serializable]
    public class MaterialFunction 
    {
        public string name;
        public int materialIndex;
        public AMaterialApplyCondition applyCondition;
    }
}