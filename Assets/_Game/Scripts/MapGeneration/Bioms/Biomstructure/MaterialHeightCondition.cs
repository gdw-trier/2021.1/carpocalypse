using UnityEngine;

namespace MapGeneration.Bioms
{
    [CreateAssetMenu(menuName = "Data/Condition/MaterialHeight")]

    public class MaterialHeightCondition : AMaterialApplyCondition
    {
        public enum Operators { Greater, Less }
        public Operators comparisonOperator;
        public float constantNumber;

        public bool Apply(float height)
        {
            switch (comparisonOperator)
            {
                case Operators.Greater:
                    return height > constantNumber;
                case Operators.Less:
                    return height < constantNumber;
            }
            return false;
        }
    }
}