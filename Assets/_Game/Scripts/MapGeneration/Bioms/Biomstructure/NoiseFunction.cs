using UnityEngine;

namespace MapGeneration.Bioms
{
    [CreateAssetMenu(menuName = "Data/Functions/Noise")]

    public class NoiseFunction : AFunction
    {
        [Range(-30, 30)] public float offset;
        [Range(0, 150)] public float amplitude;
        [Range(3, 200)] public float periode;
        [Range(0, 1f)] public float roughness;

        public override float GetValue(Vector2 position)
        {
            float height = Mathf.PerlinNoise(10000 + position.x / periode, 10000 + position.y / periode) * amplitude;

            if (roughness > 0)
            {
                System.Random random = new System.Random((int)(position.x * 2 + position.y * 13907));
                height += (random.Next((int)(-roughness * 100), (int)(roughness * 100)) / 100f);
            }
            return height + offset;
        }
    }
}