using UnityEngine;

namespace MapGeneration.Bioms
{
    public abstract class AFunction : ScriptableObject
    {
        public abstract float GetValue(Vector2 position);
    }
}