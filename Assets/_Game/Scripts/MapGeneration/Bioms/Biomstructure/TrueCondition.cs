using UnityEngine;

namespace MapGeneration.Bioms
{
    [CreateAssetMenu(menuName = "Data/Condition/True")]

    public class TrueCondition : AHeightApplyCondition
    {
        public override float Apply(Vector2 point)
        {
            return 1;
        }
    }
}