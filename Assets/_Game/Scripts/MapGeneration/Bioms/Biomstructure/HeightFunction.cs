using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGeneration.Bioms
{
    [Serializable]
    public class HeightFunction
    {
        public string name;
        public enum applyType { Add, Substract, Overwrite}
        public AFunction baseFunction;
        public AHeightApplyCondition applyCondition;
        public applyType usedApplyType;
    }
}