using System;
using UnityEngine;

namespace MapGeneration.Bioms
{
    [Serializable]
    public class ProbFunction 
    {
        public string name;
        public GameObject prefab;
        public AProbApplyCondition applyCondition;
        [Range(0, 50)] public float averageAmmountPerChunk;
    }
}


