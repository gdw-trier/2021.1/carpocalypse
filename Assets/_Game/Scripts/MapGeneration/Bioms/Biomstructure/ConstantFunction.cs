using UnityEngine;

namespace MapGeneration.Bioms
{
    [CreateAssetMenu(menuName = "Data/Functions/Constant")]

    public class ConstantFunction : AFunction
    {
        [Range(0, 50)] public float value;
        [Range(0, 1f)] public float roughness;

        public override float GetValue(Vector2 position)
        {
            float height = value;

            if (roughness > 0)
            {
                System.Random random = new System.Random((int)(position.x * 2 + position.y * 13907));
                height += (random.Next((int)(-roughness * 100), (int)(roughness * 100)) / 100f);
            }
            return height;
        }
    }
}