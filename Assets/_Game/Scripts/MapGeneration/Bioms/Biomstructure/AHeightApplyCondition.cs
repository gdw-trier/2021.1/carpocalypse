using UnityEngine;

namespace MapGeneration.Bioms
{
    public abstract class AHeightApplyCondition : AMaterialApplyCondition
    {
        public abstract float Apply(Vector2 point);
    }
}