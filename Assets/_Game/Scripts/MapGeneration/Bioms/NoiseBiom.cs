using MapGeneration.Bioms;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGeneration
{
    [CreateAssetMenu(menuName = "Data/Bioms/Graslands")]

    public class NoiseBiom : ABiom
    {
        #region variables
        [Header("Calculates height by processing all heightFunctions")]
        public List<HeightFunction> heightFunctions;
        [Header("Takes material from first materialFunctions that is true")]
        public List<MaterialFunction> materialFunctions;

        [Header("Biom placement")] 
        [Range(0, 10000)] public float biomMinRangeFromCenter;
        [Range(100, 10000)] public float biomMaxRangeFromCenter;
        [Range(0, 100)] public float transitionRange;
        #endregion

        public override float GetHeight(Vector2 position)
        {
            float total = 0;
            foreach(HeightFunction heightFunction in heightFunctions)
            {
                float weight = heightFunction.applyCondition.Apply(position);
                if (weight>0)
                {
                    float value = heightFunction.baseFunction.GetValue(position);
                    switch (heightFunction.usedApplyType)
                    {
                        case HeightFunction.applyType.Add:
                            total += value * weight;
                            break;
                        case HeightFunction.applyType.Substract:
                            total -= value * weight;
                            break;
                        case HeightFunction.applyType.Overwrite:
                            total = total * (1- weight) + value * weight;
                            break;
                    }
                }
            }           
            return total;
        }

        public override int GetMaterial(Vector2 position, float height)
        {
            float max = 0;
            int maxValue = 0;
            foreach (MaterialFunction materialFunction in materialFunctions)
            {
                if (materialFunction.applyCondition is AHeightApplyCondition)
                {
                    float f = ((AHeightApplyCondition)materialFunction.applyCondition).Apply(position);
                    if (f > max)
                    {
                        max = f;
                        maxValue = materialFunction.materialIndex;
                        if(max == 1)
                        {
                            return maxValue;
                        }
                    }
                }
                else
                {
                    if (materialFunction.applyCondition is MaterialHeightCondition)
                    {
                        if (((MaterialHeightCondition)materialFunction.applyCondition).Apply(height))
                            return materialFunction.materialIndex;
                    }
                }
            }
            return 0;
        }

        public override float IsBiom(Vector2 position)
        {
            float alpha = Mathf.Atan2(position.y, position.x);
            alpha *= position.magnitude / 200;
            Vector2 smoothing = new Vector2(Mathf.Cos(alpha), Mathf.Sin(alpha));
            smoothing *= 100;
            position += smoothing;
            float sqrMag = position.sqrMagnitude;

            if(position.x > 230 && position.x < 240 && position.y > 485 && position.y < 495)
            {
                bool test = true;
            }

            if (sqrMag < biomMaxRangeFromCenter * biomMaxRangeFromCenter && sqrMag > biomMinRangeFromCenter * biomMinRangeFromCenter)
                return 1;

            if (sqrMag > biomMaxRangeFromCenter * biomMaxRangeFromCenter)
            {
                //map (biomMaxRangeFromCenter-transitionRange)*(biomMaxRangeFromCenter-transitionRange) -> 0  (biomMaxRangeFromCenter)*(biomMaxRangeFromCenter) -> 1
                float sqrMin = (biomMaxRangeFromCenter) * (biomMaxRangeFromCenter);
                float sqrMax = (biomMaxRangeFromCenter + transitionRange) * (biomMaxRangeFromCenter + transitionRange) - sqrMin;
                float output = 1-(sqrMag - sqrMin) / sqrMax;
                return Mathf.Clamp(output, 0, 1);
            }
            else
            {
                if(sqrMag < biomMinRangeFromCenter * biomMinRangeFromCenter)
                {
                    float sqrMin = (biomMinRangeFromCenter - transitionRange) * (biomMinRangeFromCenter - transitionRange);
                    float sqrMax = (biomMinRangeFromCenter) * (biomMinRangeFromCenter) - sqrMin;
                    return Mathf.Clamp((sqrMag - sqrMin) / sqrMax, 0, 1);
                }
                return 1;
            }
        }
    }
}