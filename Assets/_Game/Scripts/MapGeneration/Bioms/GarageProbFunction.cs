
using System.Collections.Generic;
using UnityEngine;
namespace MapGeneration.Bioms
{
    [CreateAssetMenu(menuName = "Data/Condition/GarageProb")]
    public class GarageProbFunction : AProbApplyCondition
    {
        public StreetBiom streetBiom;

        public bool Apply(Vector2 position)
        {
            position = (position + new Vector2(streetBiom.streetWidth * 250, streetBiom.streetWidth * 250)) / streetBiom.streetWidth;
            Vector2Int vector2Int = new Vector2Int((int)position.x, (int)position.y);

            if (!streetBiom.spawnedGarages.Contains(vector2Int) && streetBiom.garagePositions.Contains(vector2Int))
            {
                streetBiom.spawnedGarages.Add(vector2Int);
                return true;
            }
            return false;
        }
    }
}