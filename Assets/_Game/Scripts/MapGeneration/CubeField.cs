using MapGeneration;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CubeField : MonoBehaviour
{
    [SerializeField] private float radius;
    [SerializeField] private float minSize;
    [SerializeField] private float maxSize;
    [SerializeField] private int cubeCount;
    [SerializeField] private GameObject prefab;
    
    private List<Vector3> posTaken;

    private bool isSpawned;

    private void Start()
    {
        switch (Settings.currentSetting)
        {
            case Settings.PerformanceSettings.Low:
                cubeCount = (int) (cubeCount*0.3f);
                break;
            case Settings.PerformanceSettings.Medium:
                cubeCount = (int)(cubeCount * 0.6f);
                break;
            case Settings.PerformanceSettings.High:
                cubeCount = (int)(cubeCount * 1f);
                break;
            case Settings.PerformanceSettings.Ultra:
                cubeCount = (int)(cubeCount * 1.5f);
                break;
        }

        isSpawned = true;
        OnEnable();
    }    
    
    private void OnEnable()
    {
        if (!isSpawned)
            return;

        transform.position = new Vector3(transform.position.x,0, transform.position.z);
        posTaken = new List<Vector3>();

        //TODO!!! SET LOADING RANGE
        for (int i = 0; i < cubeCount; i++)
        {
            GameObject go = Instantiate(prefab, transform);

            go.transform.localPosition = RandomPoint();
            go.transform.localRotation = Quaternion.Euler(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));

            go.transform.localScale *= Random.Range(minSize, maxSize);
        }
    }

    private void OnDisable()
    {
        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }
    }

    private Vector3 RandomPoint()
    {
        Vector3 pos = Vector3.zero;
        int i = 0;
        do
        {
            Vector2 dir2D = Random.insideUnitCircle;
            Vector3 dir = new Vector3(dir2D.x, 0, dir2D.y);
            pos = dir * Random.Range(0, radius);
            pos.y = BiomManager.current.GetHeight(new Vector2(pos.x, pos.z) + new Vector2(transform.position.x,transform.position.z));
            i++;
        } while (PointTaken(pos) && i < 1000);

        posTaken.Add(pos);
        return pos;
    }

    private bool PointTaken(Vector3 pos)
    {
        foreach (Vector3 v in posTaken)
        {
            if (Vector3.Distance(v, pos) < (maxSize - minSize) / 2f) return true;
        }

        return false;
    }
}