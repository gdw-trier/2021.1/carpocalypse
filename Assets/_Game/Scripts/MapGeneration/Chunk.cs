using MapGeneration.Bioms;
using System;
using System.Collections;
using System.Collections.Generic;
using TriangleNet.Geometry;
using TriangleNet.Topology;
using UnityEngine;

namespace MapGeneration
{
    public class Chunk : MonoBehaviour
    {

        #region variables
        public int xPosition;
        public int zPosition;

        public static int chunkSize = 100;
        private const float distanceBetweenPoints = 1.25f;
        private const float randomOffsetAmplitude = 0.4f;

        private List<Vector3> vertices = new List<Vector3>();
        private List<Vector3> normals = new List<Vector3>();
        private List<Vector2> uvs = new List<Vector2>();
        private List<int>[] triangles = new List<int>[32];

        private List<Tuple<Vector3, GameObject>> probLocations = new List<Tuple<Vector3, GameObject>>();
        private Dictionary<GameObject, List<GameObject>> instancePool = new Dictionary<GameObject, List<GameObject>>();
        private Dictionary<GameObject, List<GameObject>> generatedPool;
        #endregion

        #region functions

        #region public functions
        public void InitChunk(int xPosition, int zPosition)
        {
            this.xPosition = xPosition;
            this.zPosition = zPosition;
        }
        public void GenerateChunk()
        {
            TriangleNet.Mesh mesh = GenerateXZMesh();
            FillActualMeshData(mesh);
            CalculateProbSpawns();
        }
        public void ApplyChunk()
        {
            Mesh chunkMesh = new Mesh();

            chunkMesh.SetVertices(vertices);
            chunkMesh.SetUVs(0, uvs);
            chunkMesh.SetNormals(normals);
            chunkMesh.subMeshCount = 32;
            for (int i = 0; i < 32; i++)
            {
                chunkMesh.SetTriangles(triangles[i], i);
            }

            vertices.Clear();
            uvs.Clear();
            normals.Clear();
            for (int i = 0; i < 32; i++)
            {
                triangles[i].Clear();
            }

            gameObject.GetComponent<MeshFilter>().mesh = chunkMesh;
            gameObject.GetComponent<MeshCollider>().sharedMesh = chunkMesh;


            //prop generation
            generatedPool = new Dictionary<GameObject, List<GameObject>>();
            foreach (var entry in probLocations)
            {
                GameObject newInstance = null;
                if (instancePool.ContainsKey(entry.Item2) && instancePool[entry.Item2].Count > 0)
                {
                    newInstance = instancePool[entry.Item2][0];
                    newInstance.transform.position = entry.Item1;
                    newInstance.transform.rotation = Quaternion.identity; 
                    newInstance.SetActive(true);
                    instancePool[entry.Item2].RemoveAt(0);
                }
                else
                {
                    newInstance = GameObject.Instantiate(entry.Item2,entry.Item1,Quaternion.identity,transform);
                }
                if (!generatedPool.ContainsKey(entry.Item2))
                    generatedPool.Add(entry.Item2, new List<GameObject>());
                generatedPool[entry.Item2].Add(newInstance);
            }
            foreach(List<GameObject> pools in instancePool.Values)
            {
                foreach(GameObject g in pools)
                {
                    Destroy(g);
                }
            }
            probLocations.Clear();
            instancePool = generatedPool;
        }

        private void OnDisable()
        {
            foreach (Transform t in transform)
            {
                t.gameObject.SetActive(false);
            }
        }
        #endregion

        #region private functions
        private TriangleNet.Mesh GenerateXZMesh()
        {
            Polygon polygon = new Polygon();
            for (float curX = xPosition - distanceBetweenPoints; curX < xPosition + chunkSize; curX+= distanceBetweenPoints)
            {
                for (float curZ = zPosition - distanceBetweenPoints; curZ < zPosition + chunkSize; curZ+= distanceBetweenPoints)
                {
                    Vector2 position = new Vector2(curX, curZ) + GetPseudoRandomOffset(curX, curZ, randomOffsetAmplitude);
                    polygon.Add(new Vertex(position.x, position.y));
                }
            }

            TriangleNet.Meshing.ConstraintOptions options = new TriangleNet.Meshing.ConstraintOptions() { ConformingDelaunay = true, SegmentSplitting = 2};
            return (TriangleNet.Mesh)polygon.Triangulate(options);
        }
        private Vector2 GetPseudoRandomOffset(float xPosition, float zPosition, float amplitude)
        {
            System.Random random = new System.Random((int)(xPosition * 2 + zPosition * 13907));
            return new Vector2(random.Next((int)(-amplitude*100), (int)(amplitude * 100)) /100f, random.Next((int)(-amplitude * 100), (int)(amplitude * 100)) / 100f);
        }

        private void FillActualMeshData(TriangleNet.Mesh mesh)
        {
            IEnumerator<Triangle> triangleEnumerator = mesh.triangles.GetEnumerator();
                    
            //init List Arrays
            for (int i = 0; i < 32; i++)
            {
                triangles[i] = new List<int>();
            }

            //asign mesh data to datastructure
            while (triangleEnumerator.MoveNext())
            {
                Triangle currentTriangle = triangleEnumerator.Current;

                if (CheckSkip(currentTriangle))
                {
                    continue;
                }

                int materialIndex = GetMaterialIndex(currentTriangle);
                float[] vertexHeights = GetVertexHeights(currentTriangle);

                Vector3 v0 = new Vector3((float)currentTriangle.vertices[2].x - xPosition, vertexHeights[2], (float)currentTriangle.vertices[2].y - zPosition);
                Vector3 v1 = new Vector3((float)currentTriangle.vertices[1].x - xPosition, vertexHeights[1], (float)currentTriangle.vertices[1].y - zPosition);
                Vector3 v2 = new Vector3((float)currentTriangle.vertices[0].x - xPosition, vertexHeights[0], (float)currentTriangle.vertices[0].y - zPosition);

                triangles[materialIndex].Add(vertices.Count);
                triangles[materialIndex].Add(vertices.Count + 1);
                triangles[materialIndex].Add(vertices.Count + 2);

                vertices.Add(v0);
                vertices.Add(v1);
                vertices.Add(v2);

                Vector3 normal = Vector3.Cross(v1 - v0, v2 - v0);
                normal.y *= 0.5f;//v0.y/GenerationConfig.currentConfig.amplitude;
                normals.Add(normal);
                normals.Add(normal);
                normals.Add(normal);

                float f = 10;
                uvs.Add(new Vector2((v0.x/ f) , (v0.z / f) ));
                uvs.Add(new Vector2((v1.x / f) , (v1.z / f) ));
                uvs.Add(new Vector2((v2.x / f) , (v2.z / f) ));
            }         
        }

        private void CalculateProbSpawns()
        {
            float averagePositions = (chunkSize / distanceBetweenPoints) * (chunkSize / distanceBetweenPoints);
            for (float curX = xPosition - distanceBetweenPoints; curX < xPosition + chunkSize; curX += distanceBetweenPoints)
            {
                for (float curZ = zPosition - distanceBetweenPoints; curZ < zPosition + chunkSize; curZ += distanceBetweenPoints)
                {
                    System.Random random = new System.Random((int)(curX * 2 + curZ * 13907));
                    Vector2 curPos = new Vector2(curX, curZ);
                    curPos.x += random.Next(-100, 100) / 110f;
                    curPos.y += random.Next(-100, 100) / 75f;


                    ABiom dominantBiom = BiomManager.current.bioms[BiomManager.current.GetDominantBiomIndex(curPos)];
                    float height = BiomManager.current.GetHeight(curPos);                   
                    //WORK HERE

                    for(int i = 0; i < dominantBiom.probFunctions.Count; i++)
                    {
                        bool apply = false;
                        if (dominantBiom.probFunctions[i].applyCondition is GarageProbFunction)
                        {
                            if (((GarageProbFunction)dominantBiom.probFunctions[i].applyCondition).Apply(curPos))
                            {
                                Vector3 position = new Vector3(curPos.x, height, curPos.y);
                                position.x = Mathf.RoundToInt(position.x / 10) * 10;
                                position.z = Mathf.RoundToInt(position.z / 10) * 10;
                                //TODO position fixing
                                probLocations.Add(Tuple.Create(new Vector3(position.x + 9, height, position.z + 5), dominantBiom.probFunctions[i].prefab));
                            }
                            apply = false;
                        }
                        if (dominantBiom.probFunctions[i].applyCondition is ProbGroundCondition)
                        {
                            int material = dominantBiom.GetMaterial(curPos, height);
                            apply = ((ProbGroundCondition)(dominantBiom.probFunctions[i].applyCondition)).Apply(material);
                        }else if(dominantBiom.probFunctions[i].applyCondition is MaterialHeightCondition)
                        {
                            apply = ((MaterialHeightCondition)(dominantBiom.probFunctions[i].applyCondition)).Apply(height);
                        } else if (dominantBiom.probFunctions[i].applyCondition is AHeightApplyCondition)
                        {
                            apply = 1 == ((AHeightApplyCondition)(dominantBiom.probFunctions[i].applyCondition)).Apply(curPos);
                        }

                        float modifiedAverageValue = dominantBiom.probFunctions[i].averageAmmountPerChunk;
                        switch (Settings.currentSetting)
                        {
                            case Settings.PerformanceSettings.Low:
                                modifiedAverageValue = (int)(modifiedAverageValue * 0.3f);
                                break;
                            case Settings.PerformanceSettings.Medium:
                                modifiedAverageValue = (int)(modifiedAverageValue * 0.75f);
                                break;
                            case Settings.PerformanceSettings.High:
                                modifiedAverageValue = (int)(modifiedAverageValue * 1f);
                                break;
                            case Settings.PerformanceSettings.Ultra:
                                modifiedAverageValue = (int)(modifiedAverageValue * 1.5f);
                                break;
                        }

                        if (apply && random.Next(0, 10000) / 10000f < dominantBiom.probFunctions[i].averageAmmountPerChunk/ averagePositions) //TODO!!!
                        {
                            probLocations.Add(Tuple.Create(new Vector3(curPos.x, height, curPos.y), dominantBiom.probFunctions[i].prefab));
                            break;
                        }
                    }
                }
            }
        }
        private bool CheckSkip(Triangle triangle) 
        {
            Vector2 vertex_0 = new Vector2((float)triangle.vertices[0].x, (float)triangle.vertices[0].y);
            Vector2 vertex_1 = new Vector2((float)triangle.vertices[1].x, (float)triangle.vertices[1].y);
            Vector2 vertex_2 = new Vector2((float)triangle.vertices[2].x, (float)triangle.vertices[2].y);

            int edgeValue_0 = CheckEdgeVertex(vertex_0);
            int edgeValue_1 = CheckEdgeVertex(vertex_1);
            int edgeValue_2 = CheckEdgeVertex(vertex_2);

            if (edgeValue_0 == -1 || edgeValue_1 == -1 || edgeValue_2 == -1)
                return false;
            if (edgeValue_0 == edgeValue_1 && edgeValue_1 == edgeValue_2)
                return true;

            //Calculate one angle of the triangle using cosine rule 
            float alpha = Mathf.Acos(
                    (Vector2.SqrMagnitude(vertex_0 - vertex_2) + Vector2.SqrMagnitude(vertex_0 - vertex_1) - Vector2.SqrMagnitude(vertex_1 - vertex_2)) / 
                    (2 * (vertex_0 - vertex_2).magnitude * (vertex_0 - vertex_1).magnitude));
            return alpha < 0.3f || alpha > 2.8f;

        }
        private int CheckEdgeVertex(Vector2 vector)
        {

            vector.x = vector.x - xPosition;
            vector.y = vector.y - zPosition;

            if (vector.x > chunkSize - distanceBetweenPoints - randomOffsetAmplitude)
                return 0;// 0;// 1;
            if (vector.y > chunkSize - distanceBetweenPoints - randomOffsetAmplitude)
                return 1;// 1;// 3;
            if (vector.x < -distanceBetweenPoints + randomOffsetAmplitude)
                return 2;// 0;// 1;
            if (vector.y < -distanceBetweenPoints + randomOffsetAmplitude)
                return 3;// 3;// 1;// 3;
            return -1;
        }
        private int GetMaterialIndex(Triangle triangle)
        {
            Vector2 vector_0 = new Vector2((float)triangle.vertices[0].x, (float)triangle.vertices[0].y);
            Vector2 vector_1 = new Vector2((float)triangle.vertices[1].x, (float)triangle.vertices[1].y);
            Vector2 vector_2 = new Vector2((float)triangle.vertices[2].x, (float)triangle.vertices[2].y);

            Vector2 centerPos = (vector_0 + vector_1 + vector_2) / 3;
            return BiomManager.current.GetMaterialIndex(centerPos, BiomManager.current.GetHeight(centerPos));
        }
        private float[] GetVertexHeights(Triangle triangle)
        {
            Vector2 vector_0 = new Vector2((float)triangle.vertices[0].x, (float)triangle.vertices[0].y);
            Vector2 vector_1 = new Vector2((float)triangle.vertices[1].x, (float)triangle.vertices[1].y);
            Vector2 vector_2 = new Vector2((float)triangle.vertices[2].x, (float)triangle.vertices[2].y);

            float height_0 = BiomManager.current.GetHeight(vector_0);
            float height_1 = BiomManager.current.GetHeight(vector_1);
            float height_2 = BiomManager.current.GetHeight(vector_2);

            return new float[] { height_0, height_1, height_2 };
        }
        #endregion
        #endregion
    }
}
