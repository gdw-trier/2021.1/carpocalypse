using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Probs")]
public class AProb : ScriptableObject
{
    public GameObject prefab;

    [Header("Settings for generation")] 
    public List<int> allowedUndergrounds;
    [Range(0, 50)]public float averageAmmountPerChunk;
}
