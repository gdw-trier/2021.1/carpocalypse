using System;
using System.Collections;
using Cinemachine;
using Controls;
using Data;
using MapGeneration;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class Garage : MonoBehaviour
{
    [SerializeField] private Transform player;
    private GarageController garageController;
    private CinemachineVirtualCamera camera;
    private GameObject car;
    public Transform pos;

    private bool flag;
    private bool flag2;

    private void Awake()
    {
        if (!player)
            player = FindObjectOfType<PlayerInputProvider>().transform;

        garageController = GetComponent<GarageController>();
        camera = GetComponentInChildren<CinemachineVirtualCamera>();
        camera.LookAt = player;
    }

    private void Update()
    {
        if (!flag)
        {
            Vector3 pos = transform.position;
            pos.y = BiomManager.current.GetHeight(new Vector2(pos.x, pos.z));
            transform.position = pos;
            flag = true;
        }

        if ((pos.position - player.position).magnitude < 3 && !flag2)
        {
            flag2 = true;
            Check();
        }
    }

    private void Check()
    {
        car = player.gameObject;
        camera.Priority = 2;
        player.GetComponent<PlayerInputProvider>().enabled = false;
        player.gameObject.GetComponentInParent<CarController>().ChangeInputProvider(garageController);
        garageController.car = player.gameObject.GetComponentInParent<Rigidbody>();
        StartCoroutine(ShowGarage());
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private IEnumerator ShowGarage()
    {
        yield return new WaitForSeconds(2);

        car.GetComponent<Rigidbody>().detectCollisions = false;

        yield return new WaitForSeconds(1);


        CarModule[] modules = car.GetComponentsInChildren<CarModule>();

        foreach (CarModule module in modules)
        {
            module.OnBreak();
            yield return new WaitForSeconds(Random.Range(0.25f, 0.5f));
        }

        yield return new WaitForSeconds(1);

        GameManager.Instance.ResetHighscore();
        SceneManager.LoadScene("Score");
    }
}